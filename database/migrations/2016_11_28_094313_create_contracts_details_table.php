<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contract_id', false, true);
            $table->string('rentplus_id');
            $table->string('article_key');
            $table->string('module_number');
            $table->string('comment')->nullable();
            $table->string('type');
            $table->string('name');
            $table->string('name_translated');
            $table->string('amount');
            $table->string('sign');
            $table->string('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts_details');
    }
}

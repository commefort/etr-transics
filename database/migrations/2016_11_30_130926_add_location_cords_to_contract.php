<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationCordsToContract extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->string('address_lng')->after('address_title')->nullable();
            $table->string('address_lat')->after('address_title')->nullable();
            $table->integer('driver_in', false, true)->after('date_in')->nullable();
            $table->integer('driver_out', false, true)->after('date_out')->nullable();
            $table->string('status')->default('unplanned')->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('address_lng');
            $table->dropColumn('address_lat');
            $table->dropColumn('driver_in');
            $table->dropColumn('driver_out');
            $table->dropColumn('status');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDriverFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->string('fleet')->after('plate');
            $table->string('vehicle_id')->after('fleet');
            $table->string('transics_id')->after('vehicle_id');
            $table->string('current_kms')->after('transics_id');
            $table->enum('status', ['operational', 'maintenance', 'inactive'])->after('current_kms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->dropColumn('fleet');
            $table->dropColumn('vehicle_id');
            $table->dropColumn('transics_id');
            $table->dropColumn('current_kms');
            $table->dropColumn('status');
        });
    }
}

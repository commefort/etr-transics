<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContractFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function(Blueprint $table){
            $table->double('price')->after('date_out');
            $table->boolean('delivery')->after('location_contact_phone');
            $table->integer('delivery_driver_id', false, true)->after('delivery');
            $table->boolean('pickup')->after('location_contact_phone');
            $table->integer('pickup_driver_id', false, true)->after('pickup');
            $table->renameColumn('contact_name', 'customer_contact_name');
            $table->renameColumn('contact_phone', 'customer_contact_phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function(Blueprint $table){
            $table->dropColumn('price');
            $table->dropColumn('delivery');
            $table->dropColumn('pickup');
            $table->renameColumn('customer_contact_name', 'contact_name');
            $table->renameColumn('customer_contact_phone', 'contact_phone');
        });
    }
}

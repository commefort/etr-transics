<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rentplus_key');
            $table->string('venture_number');
            $table->string('warehouse_number');
    
    
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('postal')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('language')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id', false, true);
            $table->string('order_number');
            $table->text('address_title');
            $table->string('address_street');
            $table->string('address_location');
            $table->string('address_country');
            $table->string('contact_id');
            $table->string('contact_name');
            $table->string('contact_phone')->nullable();
            $table->string('location_contact_name');
            $table->string('location_contact_phone')->nullable();
            $table->timestamp('date_in')->nullable();
            $table->timestamp('date_out')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}

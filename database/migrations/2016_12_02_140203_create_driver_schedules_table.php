<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id', false, true);
            $table->string('date');
            $table->enum('type', ['contract', 'task']);
            $table->text('meta');
            $table->enum('slot', ['created','morning', 'afternoon']);
            $table->enum('status', ['placed', 'planned', 'synced']);
            $table->integer('order', false, true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_schedules');
    }
}

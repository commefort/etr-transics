<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'
 */
Route::get('macros', 'FrontendController@macros')->name('macros');

/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'FrontendController@index')->name('index');
    Route::get('calendar', 'FrontendController@calendar')->name('calendar');
    Route::get('planner', 'FrontendController@planner')->name('planner');
    
    Route::get('day/{day}', [
        'uses' => 'ContractController@forDay',
        'laroute' => true
    ])->name('day');
    
    Route::post('day/{day}/duplicate', [
        'uses' => 'ContractController@postDuplicateContract',
        'laroute' => true
    ])->name('contract.duplicate');
    
    Route::post('day/{day}/duplicate/delete', [
        'uses' => 'ContractController@postDeleteContractDuplicate',
        'laroute' => true
    ])->name('contract.remove');
    
    Route::get('refresh/{date}', [
        'uses' => 'ContractController@getRefresh',
        'laroute' => true
    ])->name('refresh');
    
    Route::get('calendar/{date}', [
        'uses' => 'ContractController@getCalendar',
        'laroute' => true
    ])->name('calendar.day');
    
    Route::post('coordinates/{contract}', [
        'uses' => 'ContractController@updateCoordinates',
        'laroute' => true
    ])->name('contract.coordinates');
    
    Route::post('{date}/contracts', [
        'uses' => 'ContractController@updateDriver',
        'laroute' => true
    ])->name('contract.driver');
    
    Route::post('{date}/driver/{driver}', [
        'uses' => 'ContractController@updateSchedule',
        'laroute' => true
    ])->name('driver.schedule');
    
	Route::group(['namespace' => 'User', 'as' => 'user.'], function() {
		/**
		 * User Dashboard Specific
		 */
		Route::get('dashboard', 'DashboardController@index')->name('dashboard');

		/**
		 * User Account Specific
		 */
		Route::get('account', 'AccountController@index')->name('account');

		/**
		 * User Profile Specific
		 */
		Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
	});
});
<?php
/**
 * Driver Management
 */
Route::group([
    'middleware' => 'access.routeNeedsPermission:manage-users',
], function ()
{
    Route::group(['namespace' => 'Activities'], function ()
    {
        /**
         * For DataTables
         */
        Route::post('activity/get', 'ActivityTableController')->name('activities.get');
        
        /**
         * User CRUD
         */
        Route::resource(
            'activities',
            'ActivityController',
            [
                'parameters' => [
                    'activities' => 'transics_activity',
                ],
            ]);
    });
});
<?php
    /**
     * Driver Management
     */
    Route::group([
        'middleware' => 'access.routeNeedsPermission:manage-users',
    ], function() {
        Route::group(['namespace' => 'Cars'], function() {
            /**
             * For DataTables
             */
            Route::post('car/get', 'CarsTableController')->name('car.get');
            
            /**
             * User CRUD
             */
            Route::resource('car', 'CarController', ['parameters' => [
                'car' => 'driver'
            ]]);
            
            /**
             * Deleted User
             */
            Route::group(['prefix' => 'car/{deletedCar}'], function() {
                Route::get('delete', 'CarController@delete')->name('car.delete-permanently');
            });
        });
    });
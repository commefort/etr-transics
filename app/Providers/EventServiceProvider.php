<?php

namespace App\Providers;

use App\Listeners\Backend\ActivityEventListener;
use App\Listeners\Backend\DriverEventListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 * @package App\Providers
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [];

	/**
	 * Class event subscribers
	 * @var array
	 */
	protected $subscribe = [
		/**
		 * Frontend Subscribers
		 */

		/**
		 * Auth Subscribers
		 */
		\App\Listeners\Frontend\Auth\UserEventListener::class,

		/**
		 * Backend Subscribers
		 */
        DriverEventListener::class,
        ActivityEventListener::class,
		/**
		 * Access Subscribers
		 */
		\App\Listeners\Backend\Access\User\UserEventListener::class,
		\App\Listeners\Backend\Access\Role\RoleEventListener::class,
	];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

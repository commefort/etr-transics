<?php

namespace App\Providers;

use App\Models\Access\User\User;
use App\Models\Contract;
use App\Models\Driver;
use App\Models\TransicsActivity;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

/**
 * Class RouteServiceProvider
 * @package App\Providers
 */
class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
		/**
		 * Register route model bindings
		 */

		/**
		 * This allows us to use the Route Model Binding with SoftDeletes on
		 * On a model by model basis
		 */
		$this->bind('deletedUser', function($value) {
			$user = new User;
			return User::withTrashed()->where($user->getRouteKeyName(), $value)->first();
		});
    
        Route::model('contract', Contract::class);
        Route::model('driver', Driver::class);
        Route::model('transics_activity', TransicsActivity::class);
    
        Route::pattern('date', '[0-9]{2}-[0-9]{2}-[0-9]{4}');
        Route::pattern('day', '[0-9]{2}-[0-9]{2}-[0-9]{4}');
    
    
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();

        $this->mapApiRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }
}

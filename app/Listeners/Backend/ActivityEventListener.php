<?php

namespace App\Listeners\Backend;

use App\Events\Backend\Activities\ActivityUpdated;

/**
 * Class ActivityEventListener
 * @package App\Listeners\Backend
 */
class ActivityEventListener
{
	/**
	 * @var string
	 */
	private $history_slug = 'Activity';
    
	/**
	 * @param $event
	 */
	public function onUpdated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.backend.activities.updated") '.$event->activity->name,
			$event->activity->id,
			'flag',
			'bg-aqua'
		);
	}

	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{

		$events->listen(
			ActivityUpdated::class,
			'App\Listeners\Backend\ActivityEventListener@onUpdated'
		);
	}
}
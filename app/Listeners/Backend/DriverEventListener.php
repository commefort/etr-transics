<?php

namespace App\Listeners\Backend;

use App\Events\Backend\Drivers\DriverCreated;
use App\Events\Backend\Drivers\DriverDeleted;
use App\Events\Backend\Drivers\DriverUpdated;

/**
 * Class DriverEventListener
 * @package App\Listeners\Backend
 */
class DriverEventListener
{
	/**
	 * @var string
	 */
	private $history_slug = 'Driver';

	/**
	 * @param $event
	 */
	public function onCreated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.backend.drivers.created") '.$event->driver->plate,
			$event->driver->id,
			'car',
			'bg-green'
		);
	}

	/**
	 * @param $event
	 */
	public function onUpdated($event) {
		history()->log(
			$this->history_slug,
			'trans("history.backend.drivers.updated") '.$event->driver->plate,
			$event->driver->id,
			'car',
			'bg-aqua'
		);
	}

	/**
	 * @param $event
	 */
	public function onDeleted($event) {
		history()->log(
			$this->history_slug,
			'trans("history.backend.drivers.deleted") '.$event->driver->plate,
			$event->driver->id,
			'car',
			'bg-maroon'
		);
	}

	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
		$events->listen(
			DriverCreated::class,
			'App\Listeners\Backend\DriverEventListener@onCreated'
		);

		$events->listen(
			DriverUpdated::class,
			'App\Listeners\Backend\DriverEventListener@onUpdated'
		);

		$events->listen(
			DriverDeleted::class,
			'App\Listeners\Backend\DriverEventListener@onDeleted'
		);

	}
}
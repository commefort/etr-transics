<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomersContact extends Model
{
    /**
     * @return Customer
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}

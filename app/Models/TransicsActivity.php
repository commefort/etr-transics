<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransicsActivity extends Model
{
    protected $fillable = ['transics_id', 'type', 'name', 'is_planning', 'color'];
    
    protected $casts = [
        'is_planning' => 'bool'
    ];
    
    public function getColorIconAttribute()
    {
        return '<i style="color: '.$this->color.'" class="fa fa-square"></i>';
    }
    
    public function getPlannableAttribute()
    {
        $icon = '<i class="fa fa-';
        $icon .= ($this->is_planning) ? 'check' : 'lock';
        return $icon . '"></i>';
    }
}
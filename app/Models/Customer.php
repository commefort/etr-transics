<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * @return CustomersContact[]
     */
    public function contacts()
    {
        return $this->hasMany(CustomersContact::class);
    }
    
    /**
     * @return Contract[]
     */
    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }
}

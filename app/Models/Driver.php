<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Driver extends Model
{
    use Notifiable;
    
    protected $fillable = ['fleet', 'vehicle_id', 'transics_id', 'current_kms', 'status', 'plate', 'email', 'hide'];
    
    protected $dates = ['created_at', 'updated_at'];
    
    public function tasks()
    {
        return $this->hasMany(DriverSchedule::class);
    }
    
    public function scopeOnDate($query, $date)
    {
        if(!is_a($date, Carbon::class))
        {
            $date = Carbon::createFromFormat('d/m/Y', $date);
        }
        
        return $query->with(['tasks' => function($q) use($date){
            return $q->whereDate('date', '=', $date->toDateString())->orderBy('order', 'ASC');
        }]);
    }
    
    public function getTransStatusAttribute()
    {
        switch($this->status)
        {
            case 'operational':
                return 'actief';
            case 'inactive';
                return 'inactief';
            case 'maintenance':
                return 'in onderhoud';
        }
    }
    
    public function getTransTypeAttribute()
    {
        switch($this->type)
        {
            case 'internal':
                return 'transics';
            case 'external';
                return 'extern';
        }
    }
    
    public function routeNotificationForMail()
    {
        return $this->email;
    }
}

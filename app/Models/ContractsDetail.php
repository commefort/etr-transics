<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractsDetail extends Model
{
    /**
     * @return Contract
     */
    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }
}

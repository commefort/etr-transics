<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class DriverSchedule extends Model
{
    use Notifiable;
    
    protected $dates = ['created_at', 'updated_at', 'date'];
    protected $fillable = ['meta', 'slot', 'order', 'activity_id'];
    
    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }
    
    public function activity()
    {
        return $this->belongsTo(TransicsActivity::class);
    }
    
    public function contract()
    {
        if($this->type == 'contract')
        {
            return Contract::find($this->meta['contract_id']);
        }
        
        return null;
    }
    protected $casts = [
        'meta' => 'array'
    ];
    
    /**
     * Route notifications for the mail channel.
     *
     * @return string
     */
    public function routeNotificationForMail()
    {
        return 'maxim@commefort.com';
        

        // @todo set the email right
        if($this->type == 'contract')
            return $this->contract()->details->last()->email;
        
        return $this->meta['email'];
    }
    
    // The number to send the SMS to
    public function routeNotificationForPlivo()
    {
        return '32475755119';
    
        $number = ($this->type == 'contract') ? $this->contract()->location_contact_phone : $this->meta['telephone'];
        
        $cleaned = preg_replace("/[^0-9+]/", "", $number);
        
        // If no international code has been defined, assume it's a belgian number
        if(!starts_with($cleaned, ['+', '00']))
            return '32' . substr($cleaned, 1);
        
        // Remove the +
        if(starts_with('+', $cleaned))
            return substr($cleaned, 1);
    
        // Remove the 00
        return substr($cleaned, 2);
    }
}

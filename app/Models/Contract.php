<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'date_in',
        'date_out'
    ];
    /**
     * @return ContractsDetail[]
     */
    public function details()
    {
        return $this->hasMany(ContractsDetail::class);
    }
    
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    
    public function contact()
    {
        return $this->belongsTo(CustomersContact::class, 'contact_id', 'rentplus_key');
    }
    
    public function schedule()
    {
        return DriverSchedule::where('meta', json_encode(['contract_id' => $this->id]))->first();
    }
}

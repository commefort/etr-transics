<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Models\CustomersContact;
use App\Models\Driver;
use App\Models\TransicsActivity;
use Carbon\Carbon;

/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // Reset cache in dev environments
        if(in_array(env('APP_ENV'), ['local', 'staging']))
        {
            \Cache::forget('orders_tomorrow');
        }
        
        // Cache these for 10 minutes
        $tomorrow = \Cache::remember('orders_tomorrow', 10, function ()
        {
            return Contract::with(['customer', 'details', 'contact'])
                           ->whereBetween('date_in',
                               [
                                   Carbon::tomorrow()->startOfDay()->toDateString(),
                                   Carbon::tomorrow()->endOfDay()->toDateString(),
                               ])
                           ->orWhereBetween('date_out', [
                               Carbon::tomorrow()->startOfDay()->toDateString(),
                               Carbon::tomorrow()->endOfDay()->toDateString(),
                           ])
                           ->get()
                           ->map(function ($contract)
                           {
                               return [
                                   'id' => $contract->id,
                                   'order_number' => $contract->order_number,
                                   'customer' => $contract->customer->rentplus_key,
                    
                                   'location' => $contract->address_title,
                                   'street' => $contract->address_street,
                                   'postal' => $contract->address_location,
                                   'country' => $contract->address_country,
                    
                                   'customer_contact' => $contract->customer_contact_name,
                                   'customer_contact_phone' => $contract->customer_contact_phone,
                                   'customer_contact_email' => ( $contract->contact !== null ) ? $contract->contact->email : '',
                    
                                   'location_contact' => $contract->location_contact_name,
                                   'location_contact_phone' => $contract->location_contact_phone,
                    
                                   'transport_in' => $contract->date_out->format('d/m/Y'),
                                   'transport_out' => $contract->date_in->format('d/m/Y'),
                                   'delivery' => $contract->delivery,
                                   'pickup' => $contract->pickup,
                    
                                   'order_details' => $contract->details->count()
                               ];
                           });
        });
        
        return view('frontend.index', compact('tomorrow'));
    }
    
    /**
     * @return \Illuminate\View\View
     */
    public function planner()
    {
        $drivers = Driver::select(['id', 'fleet', 'plate', 'type'])->where('hide', false)->get()->map(function($driver){
            return $driver->toArray() + ['planning' => []];
        });
        
        $activities = TransicsActivity::where('is_planning', true)->get()->map(function ($act){
            return [
                'id' => $act->transics_id,
                'name' => $act->name,
                'color' => $act->color
            ];
        });
        
        return view('frontend.planner', compact('drivers', 'activities'));
    }
    
    /**
     * @return \Illuminate\View\View
     */
    public function calendar()
    {
        return view('frontend.calendar');
    }
}

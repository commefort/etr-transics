<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Jobs\SyncDriverPlanning;
use App\Models\ContractsDetail;
use App\Models\TransicsActivity;
use Arcanedev\Support\Bases\Model;
use Illuminate\Http\Request;
use App\Jobs\RefreshDayInRentplus;
use App\Models\Contract;
use App\Models\Driver;
use App\Models\DriverSchedule;
use App\Transformers\ContractPlanning;
use App\Transformers\DriverPlanning;
use Carbon\Carbon;

/**
 * Class ContractController
 * @package App\Http\Controllers
 */
class ContractController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function forDay($date)
    {
        $date = Carbon::createFromFormat('d-m-Y', $date);
        
        $contracts = Contract::with(['customer', 'details', 'contact'])
                             ->whereBetween('date_in',
                                 [
                                     $date->startOfDay()->toDateString(),
                                     $date->endOfDay()->toDateString(),
                                 ]
                             )
                             ->orWhereBetween('date_out',
                                 [
                                     $date->startOfDay()->toDateString(),
                                     $date->endOfDay()->toDateString(),
                                 ]
                             )
                             ->get();
        
        $contracts->each(function ($contract) use ($contracts)
        {
            // If we're responsible for delivery & pickup
            if ($contract->delivery && $contract->pickup)
            {
                // If delivery happens on the same day as pickup
                if ($contract->date_in == $contract->date_out)
                {
                    // Add a copy witch a different delivery date
                    // This way the copy wil be processed as a pickup
                    $extraContract = clone $contract;
                    $extraContract->date_out = $extraContract->date_out->subDay();
                    $contracts->add($extraContract);
                    
                    // Make sure the current one isn't confused for a pickup
                    $contract->date_in = $contract->date_in->subDay();
                }
            }
        });
        
        $duplicateContracts = [];
        $contracts = fractal()->collection($contracts)
                              ->transformWith(new ContractPlanning($date->format('d/m/Y'), $duplicateContracts))
                              ->addMeta(['duplicates' => &$duplicateContracts])
                              ->toArray();
        
        return response()->json([
            'contracts' => $contracts,
            'drivers' => fractal()->collection(Driver::onDate($date->format('d/m/Y'))->get())
                                  ->transformWith(new DriverPlanning($date->format('d/m/Y')))
                                  ->toArray(),
        ]);
    }
    
    public function getRefresh($date)
    {
        $this->dispatch(new RefreshDayInRentplus(Carbon::createFromFormat('d-m-Y', $date)));
        
        return response()->json([
            'status' => 'success',
            'message' => 'De actie staat in de wachtrij',
        ]);
    }
    
    public function updateCoordinates(Request $request, Contract $contract)
    {
        Contract::unguard();
        $contract->update([
            'address_lat' => $request->input('lat'),
            'address_lng' => $request->input('lng'),
        ]);
        
        return response()->json([
            'status' => 'success',
            'message' => 'Coördinaten zijn bijgewerkt',
        ]);
    }
    
    public function updateDriver(\Illuminate\Http\Request $request, $date)
    {
        Contract::unguard();
        
        $meta = [];
        
        switch ($request->input('action'))
        {
            case 'add':
                
                $bonnen = [];
                
                foreach ($request->input('contracts') as $contractId => $driverType)
                {
                    $type = ( $driverType == 'delivery' ) ? 'out' : 'in';
                    $contract = Contract::find($contractId);
                    
                    $contract->update([
                        'driver_' . $type => $request->input('driver'),
                    ]);
                    
                    $bonnen[] = $contract->order_number;
                    
                    $activity = TransicsActivity::where('transics_id', ( $type == 'in' ) ? 145542 : 145541)->first();
                    
                    // Schedule it
                    $task = DriverSchedule::create([
                        'activity_id' => $activity->id,
                        'driver_id' => $request->input('driver'),
                        'type' => 'contract',
                        'date' => Carbon::createFromFormat('d-m-Y', $date),
                        'slot' => 'created',
                        'status' => 'placed',
                        'order' => 0,
                        'meta' => [
                            'contract_id' => $contract->id,
                        ],
                    ]);
                    
                    $meta[$contractId] = $task->id;
                }
                
                $message = 'Chauffeur is bijgewerkt voor de volgende bon(nen): ' . implode(', ', $bonnen);
            break;
            case 'remove':
                
                $contractId = $request->input('contract');
                
                $task = DriverSchedule::find($contractId);
                
                if ($task !== null)
                {
                    $contract = $task->contract();
                    
                    $type = ( $request->input('type') == 'delivery' ) ? 'out' : 'in';
                    // Remove the driver from the contract
                    $contract->update([
                        'driver_' . $type => null,
                    ]);
                    
                    // Remove from transics if a place_id is defined
                    if ($task->place_id != null)
                    {
                        // Send Transics actions to the queue (only delete the one task)
                        dispatch(
                            new SyncDriverPlanning(
                                $task->driver,
                                Carbon::createFromFormat('d-m-Y', $date),
                                [], // Nothing new
                                [], // Nothing to update
                                collect([['id' => $task->id, 'place_id' => $task->place_id]]),
                                auth()->user()
                            )
                        );
                    }
                    
                    // Remove from schedule
                    $task->delete();
                    
                    $message = 'De chauffeur is verwijdert voor de volgende bon: ' . $contract->order_number . '.';
                }
                else
                {
                    // wasn't scheduled
                    $message = 'Er was een fout opgetreden.';
                    
                }
            
            break;
        }
        
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'meta' => $meta,
        ]);
    }
    
    public function updateSchedule(Request $request, $date, Driver $driver)
    {
        //dd($request->input('schedule'));
        $date = Carbon::createFromFormat('d-m-Y', $date);
        
        $submittedScheduleChanges = $request->input('schedule');
        
        $driverSchedule = DriverSchedule::where('driver_id', $driver->id)
                                        ->whereDate('date', $date->format('Y-m-d'))
                                        ->get();
        
        $keepInSchedule = [];
        $newInSchedule = [];
        
        \DB::beginTransaction();
        Model::unguard();
        foreach ($submittedScheduleChanges as $slot => $tasks)
        {
            if (count($tasks) > 0)
            {
                foreach ($tasks as $task)
                {
                    $new = false;
                    $activity = TransicsActivity::where('transics_id', $task['activity'])->first();
                    
                    switch ($task['type'])
                    {
                        case 'contract':
                            // Check if it's already in the driver's schedule
                            $search = $driverSchedule->filter(function ($value, $key) use ($task)
                            {
                                return $value->type == 'contract' && $value->meta == ['contract_id' => $task['meta']['id']];
                            });
                            
                            
                            $model = ( $search->count() == 1 ) ? $search->first() : null;
                            
                            // Create it
                            if ($model == null)
                            {
                                $model = new DriverSchedule([
                                    'driver_id' => $driver->id,
                                    'type' => 'contract',
                                    'date' => $date,
                                    'activity_id' => $activity->id,
                                    'meta' => [
                                        'contract_id' => $task['meta']['id'],
                                    ],
                                ]);
                                $new = true;
                            }
                            // If it already has a place id, update the model
                            else if (!in_array($slot, ['created', 'planned']) && $model->place_id != null)
                            {
                                $keepInSchedule[] = $model->id;
                            }
                            // Otherwise transics does not know about it yet
                            else if (!in_array($slot, ['created', 'planned']) && $model->place_id == null)
                            {
                                $new = true;
                            }
                            
                            // Update
                            $model->fill([
                                'slot' => $slot,
                                'order' => ( !in_array($slot, ['created', 'planned']) ) ? $task['order'] : 0,
                            ])->save();
                            
                            // Register as new only if it's planned during the day
                            if ($new && !in_array($slot, ['created', 'planned']))
                            {
                                $newInSchedule[] = $model->id;
                            }
                        break;
                        default:
                            $new = false;
                            // New task
                            if (!isset($task['id']) || $task['id'] == 'null')
                            {
                                $model = new DriverSchedule([
                                    'driver_id' => $driver->id,
                                    'type' => 'task',
                                    'date' => $date,
                                    'activity_id' => $activity->id,
                                    'meta' => $task['meta'],
                                ]);
                                $new = true;
                            }
                            else
                            {
                                $model = DriverSchedule::where('id', $task['id'])->where('type', '!=',
                                    'contract')->first();
                                $model->fill([
                                    'meta' => $task['meta'],
                                ]);
                                $keepInSchedule[] = $model->id;
                            }
                            
                            // Update
                            $model->fill([
                                'slot' => $slot,
                                'order' => ( !in_array($slot, ['created', 'planned']) ) ? $task['order'] : 0,
                            ])->save();
                            
                            if ($new && !in_array($slot, ['created', 'planned']))
                            {
                                $newInSchedule[] = $model->id;
                            }
                        break;
                    }
                }
            }
        }
        
        // Retrieve a list of tasks that weren't in the request
        $deleteTasks = $driverSchedule->filter(function ($task, $index) use ($keepInSchedule, $newInSchedule)
        {
            return !in_array($task->id, $keepInSchedule) && !in_array($task->id,
                    $newInSchedule) && !empty($task->place_id);
        });
        
        // These tasks should be deleted from record
        if ($deleteTasks->count() == 0)
        {
            $deleteTasks = $deleteTasks->map(function ($task, $index)
            {
                $data = [
                    'id' => $task->id,
                    'place_id' => $task->place_id,
                ];
                
                // Remove from DB
                if (!in_array($task->slot, ['created', 'planned']))
                {
                    $task->delete();
                }
                else
                {
                    $task->update(['place_id' => null]);
                }
                
                return $data;
            });
        }
        
        // Send Transics actions to the queue
        dispatch(
            ( new SyncDriverPlanning(
                $driver,
                $date,
                $newInSchedule,
                $keepInSchedule,
                $deleteTasks,
                auth()->user()
            ) )
        );
        
        \DB::commit();
        
        return response()->json([
            'status' => 'success',
            'message' => 'De actie staat in de wachtrij',
        ]);
    }
    
    public function getCalendar($date)
    {
        $date = Carbon::createFromFormat('d-m-Y', $date);
        
        return Driver::with([
            'tasks' => function ($q)
            {
                return $q->with('activity')->orderBy('order');
            },
        ])->whereHas('tasks')->onDate($date->format('d/m/Y'))->get()->map(function ($driver)
        {
            $data = [
                'plate' => $driver->plate,
                'morning' => [],
                'afternoon' => [],
                'early' => [],
                'evening' => [],
            ];
            
            $grouped = $driver->tasks->groupBy('slot');
            
            foreach ($grouped as $type => $tasks)
            {
                if (!in_array($type, ['morning', 'afternoon', 'early', 'evening']))
                {
                    continue;
                }
                
                foreach ($tasks as $task)
                {
                    if ($task->type == 'contract')
                    {
                        $contract = $task->contract();
                        
                        $def = [
                            'id' => $task->id,
                            'activity' => $task->activity_id,
                            'number' => $contract->order_number,
                            'description' => ( $contract->details->count() > 0 ) ? $contract->details->last()->name : '',
                            'type' => $task->activity->name,
                            'client' => [
                                'name' => $contract->customer->name,
                                'address' => $contract->customer->address . ' ' . $contract->customer->postal,
                                'email' => $contract->customer->email,
                                'phone' => $contract->customer->mobile,
                                'contact' => [
                                    'name' => $contract->customer_contact_name,
                                    'phone' => $contract->customer_contact_phone,
                                ],
                            ],
                            'contact' => [
                                'name' => $contract->location_contact_name,
                                'phone' => $contract->location_contact_phone,
                            ],
                            'location' => [
                                'title' => $contract->address_title,
                                'street' => $contract->address_street,
                                'postal' => $contract->address_location . ' ' . $contract->address_country,
                            ],
                            'status' => $task->transics_status,
                            'itemsCount' => $contract->details->count(),
                            'items' => $contract->details->toArray(),
                        ];
                    }
                    else
                    {
                        $def = [
                            'id' => $task->id,
                            'activity' => $task->activity_id,
                            'number' => $task->meta['title'],
                            'description' => $task->meta['text'],
                            'type' => $task->activity->name,
                            'client' => [
                                'name' => '',
                                'address' => '',
                                'email' => isset($task->meta['email']) ? $task->meta['email'] : '',
                                'phone' => '',
                                'contact' => [
                                    'name' => '',
                                    'phone' => isset($task->meta['telephone']) ? $task->meta['telephone'] : '',
                                ],
                            ],
                            'contact' => [
                                'name' => '',
                                'phone' => isset($task->meta['telephone']) ? $task->meta['telephone'] : '',
                            ],
                            'location' => [
                                'title' => 'Taak',
                                'street' => $task->meta['address'],
                                'postal' => '',
                            ],
                            'status' => $task->transics_status,
                            'itemsCount' => '?',
                            'items' => [],
                        ];
                    }
                    
                    $data[$type][] = $def;
                }
            }
            
            return $data;
        });
    }
    
    public function postDuplicateContract(Request $request, $date)
    {
        $orderNumber = $request->input('order_number');
        
        $amount = Contract::where('order_number', $orderNumber)->count();
        
        if ($amount == 4)
        {
            return response()->json([
                'status' => 'error',
                'message' => 'U kan niet meer dan 4 kopiëen hebben van dezelfde bon.',
            ]);
        }
        else if ($amount == 0)
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Het contract dat u probeert te dupliceren bestaat niet.',
            ]);
        }
        
        $original = Contract::where('order_number', $orderNumber)->first();
        
        Model::unguard();
        $newContract = Contract::create(collect($original->toArray())->except(['created_at', 'updated_at', 'driver_in', 'driver_out', 'status', 'id'])->toArray());
        
        foreach($original->details->toArray() as $detail)
        {
            ContractsDetail::create(collect($detail)->except(['created_at', 'updated_at', 'contract_id', 'id'])->prepend($newContract->id, 'contract_id')->toArray());
        }
        
    
        Model::reguard();
        
        $duplicates = [];
        return response()->json([
            'status' => 'success',
            'contract' => fractal()->item($newContract)
                                   ->transformWith(new ContractPlanning($date, $duplicates))
                                   ->toArray()
        ]);
    }
    
    public function postDeleteContractDuplicate(Request $request, $date)
    {
        $orderNumber = $request->input('order_number');
    
        $amount = Contract::where('order_number', $orderNumber)->count();
    
        if ($amount == 1)
        {
            return response()->json([
                'status' => 'error',
                'message' => 'De bon die u probeert te verwijderen is geen duplicaat.',
            ]);
        }
        else if ($amount == 0)
        {
            return response()->json([
                'status' => 'error',
                'message' => 'De bon die u probeert te verwijderen bestaat niet.',
            ]);
        }
        
        $contract = Contract::where('id', $request->input('id'))->first();
        
        if($contract == null)
        {
            return response()->json([
                'status' => 'error',
                'message' => 'De bon die u probeert te verwijderen kon niet gevonden worden.',
            ]);
        }
        
        $task = $contract->schedule();
        
        if($task != null && !in_array($task->slot, ['created', 'unplanned']))
        {
            return response()->json([
                'status' => 'error',
                'message' => 'De bon die u probeert te verwijderen is momenteel ingepland.',
            ]);
        }
        
        if($task != null)
        {
            $task->delete();
        }
        
        $contract->delete();
        
        return response()->json([
            'status' => 'success',
            'message' => 'De gedupliceerde bon is verwijdert.',
            'id' => $contract->id
        ]);
    }
}

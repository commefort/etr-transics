<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use App\Models\DriverSchedule;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.dashboard', [
            'scheduledContractsSinceYesterday' => DriverSchedule::whereDate('date', '>=', Carbon::yesterday())->get(),
            'processedContractsSinceYesterday' => DriverSchedule::whereDate('date', '<', Carbon::today())->get(),
            'internalDrivers' => Driver::where('type', 'internal')->get(),
            'externalDrivers' => Driver::where('type', 'external')->get()
        ]);
    }
}
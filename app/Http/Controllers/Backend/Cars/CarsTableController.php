<?php

namespace App\Http\Controllers\Backend\Cars;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\ManageDriverRequest;
use App\Repositories\Backend\Cars\CarRepository;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;

/**
 * Class CarsTableController
 */
class CarsTableController extends Controller
{
	/**
	 * @var CarRepository
	 */
	protected $drivers;

	/**
	 * @param CarRepository $drivers
	 */
	public function __construct(CarRepository $drivers)
	{
		$this->drivers = $drivers;
	}

	/**
	 * @param ManageUserRequest $request
	 * @return mixed
	 */
	public function __invoke(ManageDriverRequest $request) {
		return Datatables::of($this->drivers->getForDataTable())
			->addColumn('actions', function($driver) {
				$buttons = '<a href="' . route('admin.car.show', $driver) . '" class="btn btn-xs btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.view') . '"></i></a> <a href="' . route('admin.car.edit', $driver) . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
                
                if($driver->type == 'external')
                {
                    $buttons .=
                        '<a href="' . route('admin.car.destroy', $driver) . '"
                 data-method="delete"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.crud.delete') . '"
                 data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
                 class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';
                }
                
                return $buttons;
			})
            ->editColumn('status', function($driver){
                return $driver->trans_status;
            })
            ->editColumn('type', function($driver){
                return $driver->trans_type;
            })
			->make(true);
	}
}
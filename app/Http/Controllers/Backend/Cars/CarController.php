<?php

namespace App\Http\Controllers\Backend\Cars;

use App\Http\Requests\Backend\ManageDriverRequest;
use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Models\Driver;
use App\Http\Requests\Backend\StoreDriverRequest;
use App\Repositories\Backend\Cars\CarRepository;

/**
 * Class UserController
 */
class CarController extends Controller
{
    /**
     * @var CarController
     */
    protected $drivers;
    
    /**
     * @param \App\Repositories\Backend\Cars\CarRepository $drivers
     */
    public function __construct(CarRepository $drivers)
    {
        $this->drivers = $drivers;
    }
    
    /**
     * @param ManageDriverRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageDriverRequest $request)
    {
        return view('backend.cars.index');
    }
    
    /**
     * @param ManageDriverRequest $request
     *
     * @return mixed
     */
    public function create(ManageDriverRequest $request)
    {
        return view('backend.cars.create');
    }
    
    /**
     * @param StoreDriverRequest $request
     *
     * @return mixed
     */
    public function store(StoreDriverRequest $request)
    {
        $this->drivers->create(['data' => $request->except('assignees_roles')]);
        
        return redirect()->route('admin.car.index')->withFlashSuccess(trans('alerts.drivers.created'));
    }
    
    /**
     * @param \App\Models\Driver       $driver
     * @param ManageDriverRequest $request
     *
     * @return mixed
     */
    public function show(ManageDriverRequest $request, Driver $driver)
    {
        return view('backend.cars.show')
            ->withDriver($driver);
    }
    
    /**
     * @param Driver                                                  $driver
     * @param \App\Http\Requests\Backend\ManageDriverRequest $request
     *
     * @return mixed
     */
    public function edit(Driver $driver, ManageDriverRequest $request)
    {
        return view('backend.cars.edit')
            ->withDriver($driver);
    }
    
    /**
     * @param Driver              $driver
     * @param StoreDriverRequest $request
     *
     * @return mixed
     */
    public function update(Driver $driver, StoreDriverRequest $request)
    {
        $data = $request->all();
        
        $data['hide'] = $request->has('hide');
        
        $this->drivers->update($driver, compact('data'));
        
        return redirect()->route('admin.car.index')->withFlashSuccess(trans('alerts.drivers.updated'));
    }
    
    /**
     * @param \App\Models\Driver       $driver
     * @param \App\Http\Requests\Backend\ManageDriverRequest $request
     *
     * @return mixed
     */
    public function destroy(Driver $driver, ManageDriverRequest $request)
    {
        $this->drivers->delete($driver);
        
        return redirect()->route('admin.car.index')->withFlashSuccess(trans('alerts.drivers.deleted'));
    }
}
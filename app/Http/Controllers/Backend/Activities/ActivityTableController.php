<?php

namespace App\Http\Controllers\Backend\Activities;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\ManageDriverRequest;
use App\Repositories\Backend\ActivityRepository;
use App\Repositories\Backend\Cars\CarRepository;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;

/**
 * Class ActivityTableController
 */
class ActivityTableController extends Controller
{
	/**
	 * @var ActivityRepository
	 */
	protected $activities;

	/**
	 * @param ActivityRepository $activities
	 */
	public function __construct(ActivityRepository $activities)
	{
		$this->activities = $activities;
	}

	/**
	 * @param ManageUserRequest $request
	 * @return mixed
	 */
	public function __invoke(ManageDriverRequest $request) {
		return Datatables::of($this->activities->getForDataTable())
			->addColumn('actions', function($activity) {
				return '<a href="' . route('admin.activities.show', $activity) . '" class="btn btn-xs btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.view') . '"></i></a> <a href="' . route('admin.activities.edit', $activity) . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
			})
            ->editColumn('color', function($activity){
                return $activity->color_icon;
            })
            ->editColumn('is_planning', function($activity){
                return $activity->plannable;
            })
			->make(true);
	}
}
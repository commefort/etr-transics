<?php

namespace App\Http\Controllers\Backend\Activities;

use App\Http\Requests\Backend\ManageActivityRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\StoreActivityRequest;
use App\Models\TransicsActivity;
use App\Repositories\Backend\ActivityRepository;

/**
 * Class ActivityController
 */
class ActivityController extends Controller
{
    /**
     * @var ActivityRepository
     */
    protected $activities;
    
    /**
     * @param \App\Repositories\Backend\ActivityRepository $activitys
     */
    public function __construct(ActivityRepository $activitys)
    {
        $this->activities = $activitys;
    }
    
    /**
     * @param ManageActivityRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageActivityRequest $request)
    {
        return view('backend.activities.index');
    }
    
    /**
     * @param \App\Models\TransicsActivity       $activity
     * @param ManageActivityRequest $request
     *
     * @return mixed
     */
    public function show(ManageActivityRequest $request, TransicsActivity $activity)
    {
        return view('backend.activities.show')
            ->withActivity($activity);
    }
    
    /**
     * @param TransicsActivity                                                  $activity
     * @param \App\Http\Requests\Backend\ManageActivityRequest $request
     *
     * @return mixed
     */
    public function edit(TransicsActivity $activity, ManageActivityRequest $request)
    {
        return view('backend.activities.edit')
            ->withActivity($activity);
    }
    
    /**
     * @param TransicsActivity              $activity
     * @param StoreActivityRequest $request
     *
     * @return mixed
     */
    public function update(TransicsActivity $activity, StoreActivityRequest $request)
    {
        $this->activities->update($activity,
            ['data' => $request->except('assignees_roles')]);
        
        return redirect()->route('admin.activities.index')->withFlashSuccess(trans('alerts.activities.updated'));
    }
}
<?php

namespace App\Notifications;

use App\Models\Contract;
use App\Models\Driver;
use App\Models\DriverSchedule;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Plivo\PlivoChannel;
use NotificationChannels\Plivo\PlivoMessage;

class NotifyPlannedTask extends Notification implements ShouldQueue
{
    use Queueable;
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        // Only send mails when not in production
        if (env('APP_ENV') != 'production')
        {
            return ['mail'];
        }
        
        $channels = [];
        
        if (!empty( $notifiable->meta['email'] ))
        {
            $channels[] = 'mail';
        }
        if (!empty( $notifiable->meta['telephone'] ))
        {
            $channels[] = PlivoChannel::class;
        }
        
        return $channels;
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param  DriverSchedule $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        switch ($notifiable->slot)
        {
            case 'afternoon':
            case 'evening':
                $slotEn = 'afternoon';
            break;
            case 'early':
            case 'morning':
                $slotEn = 'morning';
            break;
        }
        
        switch (strtolower($notifiable->activity->name))
        {
            case 'levering':
                $activityEn = 'delivery';
            break;
            case 'retour':
                $activityEn = 'retour';
            break;
        }
        
        return ( new MailMessage )
            ->subject('Eurotaprent order: ' . $notifiable->meta['title'])
            ->view([
                'notifications::email',
                'notifications::email-plain',
            ], [
                'greeting' => 'Hello! ',
            ])
            ->line('Your order "' . $notifiable->meta['title'] . '" has been scheduled.')
            ->line($activityEn . ': in the ' . $slotEn . ' on ' . $this->localiseDays($notifiable->date->format('D d/m/y'), 'en'))
            ->line('Address: ' . $notifiable->meta['address']);
    }
    
    /**
     * Convert days to dutch
     *
     * @param string $date formatted date string.
     *
     * @return string
     */
    protected function localiseDays($date, $replacement='nl')
    {
        switch($replacement)
        {
            case 'nl':
                $days = ['ma', 'di', 'wo', 'do', 'vr', 'za', 'zo'];
            break;
            case 'en':
                $days = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su'];
            break;
            case 'fr':
                $days = ['lu', 'ma', 'me', 'je', 've', 'sa', 'di'];
            break;
        }
        return str_replace(
            ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            $days,
            $date
        );
    }
    
    /**
     * Send out
     *
     * @param DriverSchedule $notifiable
     *
     * @return PlivoMessage
     */
    public function toPlivo($notifiable)
    {
        switch ($notifiable->slot)
        {
            case 'afternoon':
            case 'evening':
                $slotEn = 'afternoon';
            break;
            case 'early':
            case 'morning':
                $slotEn = 'morning';
            break;
        }
    
        $actionEn = ( $notifiable->activity->name == 'Levering' ) ? 'delivered' : 'picked up';
        
        $enString = 'Your order with Eurotaprent  "' . $notifiable->meta['title'] . '" will be ' . $actionEn . ' on ' . $notifiable->date->format('D d/m/y') . ' in the ' . $slotEn;
        
        return ( new PlivoMessage )
            ->content( $enString);
    }
}

<?php

namespace App\Notifications;

use App\Models\Contract;
use App\Models\Driver;
use App\Models\DriverSchedule;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Plivo\PlivoChannel;
use NotificationChannels\Plivo\PlivoMessage;

class NotifyPlannedContract extends Notification implements ShouldQueue
{
    use Queueable;
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        // Only send mails when not in production
        if(env('APP_ENV') != 'production')
        {
            return [PlivoChannel::class];
        }
        
        $contract = $notifiable->contract();
        
        // If both email and phone are provided, send to both
        if (!empty( $contract->contact->email ) && !empty( $contract->contact->phone ))
        {
            return [PlivoChannel::class, 'mail'];
        }
        
        // Send to the phone or mail, which ever is provided
        return ( !empty( $contract->contact->phone ) ) ? [PlivoChannel::class] : ['mail'];
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param  DriverSchedule $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $contract = $notifiable->contract();
    
        switch ($notifiable->slot)
        {
            case 'afternoon':
            case 'evening':
                $slotEn = 'afternoon';
            break;
            case 'early':
            case 'morning':
                $slotEn = 'morning';
            break;
        }
    
        switch (strtolower($notifiable->activity->name))
        {
            case 'levering':
                $activityEn = 'delivery';
            break;
            case 'retour':
                $activityEn = 'retour';
            break;
        }
    
        return ( new MailMessage )
            ->subject('Eurotaprent order: ' . $contract->order_number)
            ->view([
                'notifications::email',
                'notifications::email-plain',
            ], [
                'greeting' => 'Hello!',
            ])
            ->line('Your order "' . $contract->order_number . '" has been scheduled.')
            ->line($activityEn . ': in the ' . $slotEn . ' on ' . $this->localiseDays($notifiable->date->format('D d/m/y'), 'en'))
            ->line('Address: ' . $contract->address_street . ' '. $contract->address_location . ' ' . $contract->address_country);
    }
    
    /**
     * Convert days to dutch
     *
     * @param string $date formatted date string.
     *
     * @return string
     */
    protected function localiseDays($date, $replacement='nl')
    {
        switch($replacement)
        {
            case 'nl':
                $days = ['ma', 'di', 'wo', 'do', 'vr', 'za', 'zo'];
            break;
            case 'en':
                $days = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su'];
            break;
            case 'fr':
                $days = ['lu', 'ma', 'me', 'je', 've', 'sa', 'di'];
            break;
        }
        return str_replace(
            ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            $days,
            $date
        );
    }
    
    /**
     * Send out
     * @param DriverSchedule $notifiable
     *
     * @return PlivoMessage
     */
    public function toPlivo($notifiable)
    {
        $contract = $notifiable->contract();
    
        switch ($notifiable->slot)
        {
            case 'afternoon':
            case 'evening':
                $slotEn = 'afternoon';
            break;
            case 'early':
            case 'morning':
                $slotEn = 'morning';
            break;
        }
    
        $actionEn = ( $notifiable->activity->name == 'Levering' ) ? 'delivered' : 'picked up';
    
        $enString = 'Your order with Eurotaprent  "' . $contract->order_number . '" will be ' . $actionEn . ' on ' . $notifiable->date->format('D d/m/y') . ' ' . $slotEn;
    
        return ( new PlivoMessage )
            ->content($enString);
    }
}

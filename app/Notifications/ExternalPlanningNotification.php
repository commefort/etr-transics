<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ExternalPlanningNotification extends Notification
{
    use Queueable;
    /**
     * @var array
     */
    public $planning;
    /**
     * @var string
     */
    public $plate;
    
    /**
     * Create a new notification instance.
     *
     * @param $planning
     * @param $plate
     */
    public function __construct($planning, $plate)
    {
        $this->planning = $planning;
        $this->plate = $plate;
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return ( new MailMessage )
            ->view([
                'notifications::planning',
                'notifications::email-plain',
            ], [
                'slots' => $this->planning
            ])
            ->subject('Planning Eurotaprent ' .Carbon::tomorrow()->format('d/m/y'))
            ->greeting('')
            ->line('Here’s the planning for tomorrow for vehicle "'.$this->plate.'":');
    }
}

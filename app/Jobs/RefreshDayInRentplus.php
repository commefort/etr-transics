<?php

namespace App\Jobs;

use App\Events\RefreshRentPlus;
use App\Models\Contract;
use App\Models\ContractsDetail;
use App\Models\Customer;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RefreshDayInRentplus implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var \Carbon\Carbon
     */
    public $date;
    
    /**
     * Create a new job instance.
     *
     * @param \Carbon\Carbon $date
     */
    public function __construct(Carbon $date)
    {
        $this->date = $date;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Tell the user we're starting to import the contracts for the provided date
        event(new RefreshRentPlus('started', $this->date->format('d-m-y')));
        
        try
        {
            $success = $this->importDayFromRentPlus();
            
            if ($success === true)
            {
                event(new RefreshRentPlus('completed', $this->date->format('d-m-y')));
                
                return;
            }
        } catch (\Exception $e)
        {
            $success = false;
            \Log::error($e);
        }
        finally
        {
            if ($success === false)
            {
                \Log::alert('Rent+ refresh mislukt voor datum ' . $this->date->format('d-m-y'));
                event(new RefreshRentPlus('failed', $this->date->format('d-m-y')));
            }
        }
    }
    
    protected function importDayFromRentPlus()
    {
        $client = new Client();
        
        $param = [
            'dateToConsider' => $this->date->format('Ymd'),
            'startDate' => $this->date->format('Y-m-d'),
            'endDate' => $this->date->format('Y-m-d'),
            'showOffer' => 'true',
            'showActiveOption' => 'true',
            'showTimeOutOption' => 'true',
            'showOrderNotToPrepare' => 'true',
            'showOrderToPrepare' => 'true',
            'showOrderPrepareListPrinted' => 'true',
            'showOrderScanBegan' => 'true',
            'showOrderScanEnded' => 'true',
            'showOrderPrepareManuallyEnded' => 'true',
            'showDelivered' => 'true',
            'showDeliveredLate' => 'true',
            'showReturnParked' => 'true',
            'showCancel' => 'true',
            'showReturnDone' => 'true',
            'locactionCode' => '',
            'wareHouseNumber' => '01'
        ];
        
        $string = [];
        
        foreach ($param as $key => $val)
        {
            $string[] = $key . '=' . $val;
        }
    
        try {
            $body = $client->get('http://mail.eurotaprent.be/RentPlusSetup/RentPlusServices.asmx/GetCurrentOrders?' . implode('&', $string))->getBody()->getContents();
        }
        catch (ClientException $e)
        {
            \Log::notice('Request failed, trying again (current orders)', $e);
            $this->release();
            return;
        }
        
        
        $replace = [
            ' xmlns="www.rentplus.be/webservices/"',
            ' xmlns:mstns="http://tempuri.org/dsCustomerList.xsd" xmlns="http://tempuri.org/dsCustomerList.xsd"',
            ' xmlns:mstns="www.rentplus.be/webservices/CustomerInfo.xsd" xmlns="www.rentplus.be/webservices/CustomerInfo.xsd"',
            ' xmlns:mstns="www.rentplus.be/webservices/ArticleList.xsd"',
            ' xmlns="www.rentplus.be/webservices/ArticleList.xsd"',
            ' xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata"',
            ' targetNamespace="http://tempuri.org/dsCustomerList.xsd"',
            ' targetNamespace="www.rentplus.be/webservices/ArticleList.xsd"',
            ' targetNamespace="www.rentplus.be/webservices/CustomerInfo.xsd"',
            ' xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1"',
            ' xmlns="http://tempuri.org/dsCustomerList.xsd"',
            ' xmlns="www.rentplus.be/webservices/ArticleInfo.xsd"',
            ' xmlns="www.rentplus.be/webservices/Order.xsd"',
            ' xmlns="www.rentplus.be/webservices/CustomerInfo.xsd"',
        ];
        
        $data = simplexml_load_string(str_replace($replace, '', $body), 'SimpleXMLElement',
            LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NSCLEAN)->children()[1];
        
        $orders = $data->children()[0];
        
        if ($orders == null || count($orders) == 0)
        {
            
            event(new RefreshRentPlus('empty', $this->date->format('d-m-y')));
            
            return null;
        }
        
        Model::unguard();
        
        $errorContracts = [];
        
        $i = 0;
        
        foreach ($orders as $order)
        {
            $customer = Customer::where('rentplus_key', $order->Key)->first();
            
            if ($customer == null)
            {
                // Can't find customer
                $errorContracts[] = (string)$order->Order_Number;
                continue;
            }
            
            // Create or update the contract
            $contract = Contract::updateOrCreate(
                [
                    'order_number' => $order->Order_Number,
                    'customer_id' => $customer->id,
                ],
                [
                    'address_title' => $order->Order_Customer_Reference,
                    'address_street' => $order->Delivery_Street_Nr,
                    'address_location' => $order->Delivery_Postal_Code . ' ' . $order->Delivery_City,
                    'address_country' => $order->Delivery_Country,
                    'contact_id' => $order->Contact_Key,
                    'customer_contact_name' => $order->Contact_Name . ' ' . $order->Contact_First_Name,
                    'customer_contact_phone' => empty( $order->Contact_Mobile_Phone_Number ) ? null : $order->Contact_Mobile_Phone_Number[0],
                    'location_contact_name' => $order->Location_Contact_Name . ' ' . $order->Location_Contact_First_Name,
                    'location_contact_phone' => empty( $order->Location_Contact_Mobile_Phone_Number ) ? null : $order->Location_Contact_Mobile_Phone_Number[0],
                    'price' => $order->Price_Sales || 0.0,
                    'delivery' => (bool) $order->Delivery_Done_By_Us,
                    'pickup' => (bool) $order->Recall_Done_By_Us,
                    'date_in' => Carbon::createFromFormat(Carbon::ATOM, $order->Date_Transport_In),
                    'date_out' => Carbon::createFromFormat(Carbon::ATOM, $order->Date_Transport_Out),
                ]
            );
    
            // Set the address
            $success = $this->getContractCoordinates($contract, $client);
            
            if(!$success)
            {
                $this->release();
                return;
            }
    
            // Set the details
            $orderDetails = $this->normaliseBody(
                $client->get('http://mail.eurotaprent.be/RentPlusSetup/RentPlusServices.asmx/GetOrderDetail?orderNumber=' . $order->Order_Number . '&ExternalOrderNumber=')
                       ->getBody()
                       ->getContents()
            )->children()[0];
            
            // We're reindexing, remove all existing articles
            $contract->details()->delete();
            
            // Loop over the articles
            foreach ($orderDetails->OrderLine as $article)
            {
                // Add the article
                ContractsDetail::create([
                    'contract_id' => $contract->id,
                    'rentplus_id' => $article->_Item_id,
                    'name' => $article->Article_Name,
                    'article_key' => $article->Article_Key,
                    'module_number' => $article->ModuleNumber,
                    'comment' => $article->Comment || null,
                    'type' => $article->Article_Type,
                    'name_translated' => $article->Article_Name_Translated,
                    'amount' => $article->Number,
                    'sign' => $article->Sign,
                    'price' => $article->Price_Sales,
                ]);
            }
        }
        
        if(count($errorContracts) > 0)
        {
            event(new RefreshRentPlus('customers.missing', $this->date, $errorContracts));
        }
        return true;
    }
    
    protected function getContractCoordinates(Contract $contract, $client)
    {
        // Don't update if lng/lat was already set
        if (empty( $contract->address_lng ))
        {
            try {
                $body = $client->get('https://maps.googleapis.com/maps/api/geocode/json?language=nl&address=' . urlencode($contract->address_street . ' ' . $contract->address_postal . ' ' . $contract->address_country) . '&key=' . env('GMAPS_KEY'))->getBody()->getContents();
            }
            catch (ClientException $e)
            {
                \Log::notice('Request failed (coordinates), trying again', $e);
                return false;
            }
            $response = json_decode($body);
            
            if (count($response->results) > 0)
            {
                $contract->forceFill([
                    'address_lng' => $response->results[0]->geometry->location->lng,
                    'address_lat' => $response->results[0]->geometry->location->lat,
                ]);
                $contract->save();
                
                return true;
            }
        }
        return true;
    }
    
    protected function normaliseBody($body)
    {
        $replace = [
            ' xmlns="www.rentplus.be/webservices/"',
            ' xmlns:mstns="http://tempuri.org/dsCustomerList.xsd" xmlns="http://tempuri.org/dsCustomerList.xsd"',
            ' xmlns:mstns="www.rentplus.be/webservices/CustomerInfo.xsd" xmlns="www.rentplus.be/webservices/CustomerInfo.xsd"',
            ' xmlns:mstns="www.rentplus.be/webservices/ArticleList.xsd"',
            ' xmlns="www.rentplus.be/webservices/ArticleList.xsd"',
            ' xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata"',
            ' targetNamespace="http://tempuri.org/dsCustomerList.xsd"',
            ' targetNamespace="www.rentplus.be/webservices/ArticleList.xsd"',
            ' targetNamespace="www.rentplus.be/webservices/CustomerInfo.xsd"',
            ' xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1"',
            ' xmlns="http://tempuri.org/dsCustomerList.xsd"',
            ' xmlns="www.rentplus.be/webservices/ArticleInfo.xsd"',
            ' xmlns="www.rentplus.be/webservices/Order.xsd"',
            ' xmlns="www.rentplus.be/webservices/CustomerInfo.xsd"',
        ];
        
        return simplexml_load_string(str_replace($replace, '', $body), 'SimpleXMLElement',
            LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NSCLEAN)->children()[1];
    }
}

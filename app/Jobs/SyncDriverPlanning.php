<?php

namespace App\Jobs;

use App\Events\FinishDriverSync;
use App\Models\Access\User\User;
use App\Models\Driver;
use App\Models\DriverSchedule;
use App\Notifications\NotifyPlannedContract;
use App\Notifications\NotifyPlannedTask;
use Barryvdh\Debugbar\Twig\Extension\Debug;
use Carbon\Carbon;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use SoapClient;
use stdClass;

class SyncDriverPlanning implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var \App\Models\Driver
     */
    public $driver;
    /**
     * @var string
     */
    public $date;
    /**
     * @var array
     */
    public $newToSchedule;
    /**
     * @var array
     */
    public $updateInSchedule;
    /**
     * @var \Illuminate\Support\Collection
     */
    public $removeFromSchedule;
    /**
     * @var \App\Models\Access\User\User
     */
    public $user;
    
    /**
     * Create a new job instance.
     *
     * @param \App\Models\Driver           $driver
     * @param \Carbon\Carbon               $date
     * @param array                        $newToSchedule      List of ids
     * @param array                        $updateInSchedule   List of ids
     * @param Collection                   $removeFromSchedule Collection containing id & place_id
     * @param \App\Models\Access\User\User $user
     */
    public function __construct(
        Driver $driver,
        Carbon $date,
        $newToSchedule,
        $updateInSchedule,
        Collection $removeFromSchedule,
        User $user
    ) {
        $this->driver = $driver;
        $this->date = $date;
        $this->newToSchedule = $newToSchedule;
        $this->updateInSchedule = $updateInSchedule;
        $this->removeFromSchedule = $removeFromSchedule;
        $this->user = $user;
    }
    
    protected $login;
    
    /**
     * @param $wsdl
     *
     * @return \SoapClient
     */
    protected function setupClient($wsdl)
    {
        $options = [
            'cache_wsdl' => 0,
        ];
        
        /* Create Soap client */
        $client = new SoapClient($wsdl, $options);
        
        /* Create login object */
        $this->login = new stdClass();
        $this->login->DateTime = date(DateTime::W3C);
        $this->login->Dispatcher = env('TRANSICS_DISPATCHER');
        $this->login->Password = env('TRANSICS_PASSWORD');
        $this->login->SystemNr = env('TRANSICS_SYSTEMNR');
        $this->login->ApplicationName = 'EuroTapRent planner';
        $this->login->ApplicationVersion = '1.0';
        $this->login->PcName = $this->user->name;
        $this->login->Integrator = env('TRANSICS_INTEGRATOR');
        $this->login->Language = 'NL';
        $this->login->Version = '0';
        
        return $client;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // external driver's don't need this
        if ($this->driver->type == 'external')
        {
            // Make sure contacts are notified.
            if (count($this->newToSchedule) > 0)
            {
                foreach ($this->newToSchedule as $schedule)
                {
                    $schedule = DriverSchedule::find($schedule);
                    
                    if ($schedule->type == 'contract')
                    {
                        $contract = $schedule->contract();
                        
                        if ($contract->contact != null && !empty( $contract->contact->email ))
                        {
                            // If no notification has been sent, send it
                            if (!$schedule->contact_notified)
                            {
                                $schedule->notify(new NotifyPlannedContract());
                                
                                // Mark as sent
                                $schedule->contact_notified = true;
                                $schedule->save();
                            }
                        }
                    }
                    // If it's a task that's either a delivery or pickup
                    else if ($schedule->type == 'task' && in_array($schedule->activity_id, [3, 4]))
                    {
                        // If no notification has been sent, send it
                        if (!$schedule->contact_notified)
                        {
                            $schedule->notify(new NotifyPlannedTask());
                            
                            // Mark as sent
                            $schedule->contact_notified = true;
                            $schedule->save();
                        }
                    }
                }
            }
            
            event(new FinishDriverSync('success', $this->date->format('d/m/y'), $this->driver));
            
            return true;
        }
        
        
        \Log::info('sync planning',
            [$this->newToSchedule, $this->updateInSchedule, $this->removeFromSchedule->toArray()]);
        
        $API_URL = 'https://tx-tango.tx-connect.com/IWS_ASMX/Service.asmx?WSDL';
        $client = $this->setupClient($API_URL);
        
        $success = true;
        
        // Remove items from the schedule
        if ($this->removeFromSchedule->count() > 0)
        {
            try
            {
                \DB::beginTransaction();
                $return = $this->removeContracts($client, $this->removeFromSchedule);
                \Log::info('Delete schedules', $this->removeFromSchedule->toArray());
                \DB::commit();
            } catch (\Exception $e)
            {
                \Log::alert('Update planning error', (array)$e);
                \Log::info('failed planning', [
                    'removals' => $this->removeFromSchedule,
                ]);
                $return = false;
                \DB::rollBack();
                
                // Retry in 30 seconds, API might be buggy
                $this->release(30);
            }
            
            
            if ($return === false)
            {
                $success = false;
            }
        }
        
        
        // Update the planning if needed
        if (count($this->updateInSchedule) > 0)
        {
            try
            {
                \DB::beginTransaction();
                
                $return = $this->updateContracts($client, $this->updateInSchedule);
                \DB::commit();
            } catch (\Exception $e)
            {
                \Log::alert('Update planning error', (array)$e);
                \Log::info('failed planning', [
                    'edits' => $this->updateInSchedule,
                ]);
                \DB::rollBack();
                $return = false;
                
                // Retry with only new and updates
                dispatch(new SyncDriverPlanning($this->driver, $this->date, $this->newToSchedule,
                    $this->updateInSchedule, collect([]), $this->user));
            }
            
            if ($return === false)
            {
                $success = false;
            }
        }
        
        // Add the items to the planning
        if (count($this->newToSchedule) > 0)
        {
            try
            {
                \DB::beginTransaction();
                
                $return = $this->addContracts($client, $this->newToSchedule);
                \DB::commit();
            } catch (\Exception $e)
            {
                \Log::info('failed planning', [
                    'new' => $this->newToSchedule,
                ]);
                \Log::alert('Update planning error', (array)$e);
                \DB::rollBack();
                
                // Retry with only new tasks
                dispatch(new SyncDriverPlanning($this->driver, $this->date, $this->newToSchedule, [], collect([]),
                    $this->user));
                $return = false;
            }
            
            if ($return === false)
            {
                $success = false;
            }
        }
        
        // Only send this notification if all actions were performed successfully
        if ($success === true)
        {
            // Send Pusher notification that the planning has been updated
            event(new FinishDriverSync('success', $this->date->format('d/m/y'), $this->driver));
        }
        
    }
    
    protected function generatePlaceId($driverId, $scheduleId)
    {
        return $driverId . '_' . $scheduleId . '_' . str_random(5);
    }
    
    protected function newPlace($index, DriverSchedule $schedule)
    {
        $sequence = $index * 10;
        
        // Generate and save the pace id if it does not exist
        if (empty( $schedule->place_id ) || $schedule->place_id == null)
        {
            $placeId = $this->generatePlaceId($this->driver->id, $schedule->id);
            $schedule->place_id = $placeId;
            $schedule->save();
        }
        else
        {
            $placeId = $schedule->place_id;
        }
        
        // Build comment string
        if ($schedule->type == 'contract')
        {
            $comment = 'BON: ' . $schedule->contract()->order_number;
            $comment .= "\n\n" . $schedule->contract()->address_street . "\n" . $schedule->contract()->address_location;
            $title = $schedule->contract()->address_title;
            $lat = $schedule->contract()->address_lat;
            $lng = $schedule->contract()->address_lng;
            $customerName = $schedule->contract()->customer->name;
            
            // If no notification has been sent, send it
            if (!$schedule->contact_notified)
            {
                $schedule->notify(new NotifyPlannedContract());
                
                // Mark as sent
                $schedule->contact_notified = true;
                $schedule->save();
            }
        }
        else
        {
            $comment = 'TAAK:' . $schedule->activity->name;
            $comment .= "\n\n" . $schedule->meta['address'];
            $title = $schedule->meta['title'];
            $lat = $schedule->meta['location']['lat'];
            $lng = $schedule->meta['location']['lng'];
            $customerName = '';
        }
        
        // Set the start hour
        switch ($schedule->slot)
        {
            case 'morning':
                $date = $schedule->date->hour('8')->minute('0');
            break;
            case 'afternoon':
                $date = $schedule->date->hour('13')->minute('0');
            break;
            case 'early':
                $date = $schedule->date->hour('6')->minute('0');
            break;
            case 'evening':
                $date = $schedule->date->hour('18')->minute('0');
            break;
            default:
                return;
        }
        
        // Reduce the title size, if needed
        $title = str_replace([' / ', ' /', '/ '], '/', $title);
        $title = ( strlen($title) > 46 ) ? str_limit($title, 46) : $title;
        
        return (object) [
            'CustomNr' => $schedule->id,
            'OrderSeq' => $sequence,
            'PlaceId' => $placeId,
            'JobId' => $placeId,
            'DriverDisplay' => $title,
            'DispatcherDisplay' => $title,
            'Comment' => $comment,
            'ExecutionDate' => $date->toRfc3339String(),
            'Activity' => [
                'ID' => $schedule->activity->transics_id,
            ],
            'Customer' => [
                'Code' => $this->user->name,
                'Name' => $this->user->name,
            ],
            'Position' => [
                'Longitude' => $lng,
                'Latitude' => $lat,
            ],
        ];
    }
    
    protected function cancelPlace($place)
    {
        return (object) [
            'PlanningSelectionType' => 'PLACE',
            'ID' => $place['place_id'],
        ];
    }
    
    protected function updateContracts($client, $contractIds)
    {
        // Retrieve the tasks
        $schedule = DriverSchedule::whereIn('id', $contractIds)->get();
        
        // Store the places
        $places = [];
        
        if ($schedule->count() > 0)
        {
            // Build the tasks' structure
            foreach ($schedule as $task)
            {
                // Use planning insert if no ID was provided
                if($task->place_id == null)
                {
                    $this->newToSchedule[] = $task->id;
                    continue;
                }
                
                // Register to update
                $places[] = $this->newPlace($task->order, $task);
            }
            
            // Request specific data structure
            $placeUpdate = (object) [
                'Vehicle' => [
                    'IdentifierVehicleType' => 'LICENSE_PLATE',
                    'Id' => $this->driver->plate,
                ],
                'Places' => $places,
            ];
            
            // Build the payload
            $payload = new stdClass();
            $payload->Login = $this->login;
            $payload->PlanningInsert = $placeUpdate;
            
            // Send the request to Transics
            $response = $client->Update_Planning($payload);
            
            // Check if there are any errors
            if ($response->Update_PlanningResult->Errors != '' && count((array) $response->Update_PlanningResult->Errors) > 0)
            {
                \Log::emergency('Planning is niet correct ge-update bij de chauffeur', [
                    'result' => (array) $response->Update_PlanningResult,
                    'date' => $this->date,
                    'driver' => $this->driver->toArray(),
                    'updates' => $this->updateInSchedule,
                    'new' => $this->newToSchedule,
                    'remove' => $this->removeFromSchedule,
                ]);
                
                // Send Pusher notification that the planning update failed to synchronise
                event(new FinishDriverSync('error', $this->date->format('d/m/y'), $this->driver));
                
                return false;
            }
            
            // Check if there are any warnings
            if ($response->Update_PlanningResult->Warnings != '' && count((array) $response->Update_PlanningResult->Warnings) > 0)
            {
                \Log::alert('Planning is correct ge-update bij de chauffeur, maar er zijn waarschuwingen', [
                    'result' => (array) $response->Update_PlanningResult->Warnings,
                    'date' => $this->date,
                    'driver' => $this->driver->toArray(),
                ]);
            }
            
        }
        
        return true;
    }
    
    protected function addContracts($client, $contractIds)
    {
        // Create a planning
        $schedule = DriverSchedule::whereIn('id', $contractIds)->get();
        
        // Store the places
        $places = [];
        
        if ($schedule->count() > 0)
        {
            // Group the places the driver has to go to
            foreach ($schedule as $task)
            {
                $places[] = $this->newPlace($task->order, $task);
            }
            
            // Build the specific data structure
            $placeInsert = (object) [
                'Vehicle' => [
                    'IdentifierVehicleType' => 'LICENSE_PLATE',
                    'Id' => $this->driver->plate,
                ],
                'Places' => $places,
            ];
            
            // Build the payload we're sending to transics
            $payload = new stdClass();
            $payload->Login = $this->login;
            $payload->PlanningInsert = $placeInsert;
            
            // Send the request to transics
            $response = $client->Insert_Planning($payload);
            
            // Check if there are any errors
            if (count((array) $response->Insert_PlanningResult->Errors) > 0)
            {
                if ($response->Insert_PlanningResult->Errors->Error->ErrorCode == 'ERROR_VEHICLE_NOT_A_RECEIVER')
                {
                    \Log::emergency('Planning is niet correct ge-update bij de chauffeur, onmogelijk om te doen, wagen inactief', [
                        'result' => (array) $response->Insert_PlanningResult,
                        'date' => $this->date,
                        'driver' => $this->driver->toArray(),
                        'updates' => $this->updateInSchedule,
                        'new' => $this->newToSchedule,
                        'remove' => $this->removeFromSchedule,
                    ]);
        
                    // Remove any tasks scheduled to this driver
                    \DB::table('driver_schedules')->where('driver_id', $this->driver->id)->delete();
        
                    // Hide the car
                    $this->driver->update(['hide' => 1]);
        
                    // Notify user
                    event(new FinishDriverSync('error.driver', $this->date->format('d/m/y'), $this->driver));
        
                    return false;
                }
    
                \Log::emergency('Planning is niet correct doorgestuurd naar chauffeur', [
                    'result' => (array) $response->Insert_PlanningResult,
                    'date' => $this->date,
                    'driver' => $this->driver->toArray(),
                    'updates' => $this->updateInSchedule,
                    'new' => $this->newToSchedule,
                    'remove' => $this->removeFromSchedule,
                ]);
                // Send Pusher notification that the planning update failed to synchronise
                event(new FinishDriverSync('error', $this->date->format('d/m/y'), $this->driver));
                
                return false;
            }
            
            // Check if there are any warnings
            if (count((array) $response->Insert_PlanningResult->Warnings) > 0)
            {
                \Log::alert('Planning is correct doorgestuurd naar chauffeur, maar er zijn waarschuwingen', [
                    'result' => (array) $response->Insert_PlanningResult->Warnings,
                    'date' => $this->date,
                    'driver' => $this->driver->toArray(),
                ]);
            }
        }
        
        return true;
    }
    
    protected function removeContracts($client, $contracts)
    {
        // Build the basic payload
        $payload = new stdClass();
        $payload->Login = $this->login;
        
        $errors = [];
        
        // For each deletion we have to send a request
        foreach ($contracts as $task)
        {
            // Add request specific data to payload
            $payload->PlanningItemSelection = $this->cancelPlace($task);
            
            // Send the request to transics
            $response = $client->Cancel_Planning_V2($payload);
            
            // Check if there are any errors
            if (count((array) $response->Cancel_Planning_V2Result->Errors) > 0)
            {
                \Log::emergency('Taak is niet correct verwijderd naar chauffeur', [
                    'result' => (array) $response->Cancel_Planning_V2Result,
                    'date' => $this->date,
                    'driver' => $this->driver->toArray(),
                ]);
                
                $errors[] = [
                    'place_id' => $task['place_id'],
                    'error' => (array) $response->Cancel_Planning_V2Result->Errors,
                ];
            }
            
            // Check if there are any warnings
            if (count((array) $response->Cancel_Planning_V2Result->Warnings) > 0)
            {
                \Log::alert('Planning is correct verwijderd naar chauffeur, maar er zijn waarschuwingen', [
                    'result' => (array) $response->Cancel_Planning_V2Result->Warnings,
                    'date' => $this->date,
                    'driver' => $this->driver->toArray(),
                    'updates' => $this->updateInSchedule,
                    'new' => $this->newToSchedule,
                    'remove' => $this->removeFromSchedule,
                ]);
            }
            
            \Log::info('removal', (array) $response->Cancel_Planning_V2Result);
            
            $stillExists = DriverSchedule::where('place_id', $task['place_id'])->first();
            
            // If the item was 'unplanned' we'll need to regenerate the place_id
            // since the previous one is now blocked by transics.
            if ($stillExists !== null)
            {
                $stillExists->update(['place_id' => null]);
                $stillExists->place_id = null;
            }
        }
        
        if (count($errors) > 0)
        {
            // send pusher notification about the errors
            event(new FinishDriverSync('error', $this->date->format('d/m/y'), $this->driver));
            
            return false;
        }
        
        return true;
    }
}

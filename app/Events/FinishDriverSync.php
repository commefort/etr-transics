<?php

namespace App\Events;

use App\Models\Driver;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FinishDriverSync implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;
    
    public $status;
    public $date;
    public $driver;
    
    /**
     * Create a new event instance.
     *
     * @param                    $status
     * @param                    $date
     * @param \App\Models\Driver $driver
     */
    public function __construct($status, $date, Driver $driver)
    {
        $this->status = $status;
        $this->date = $date;
        $this->driver = $driver->plate;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('rentplus');
    }
}

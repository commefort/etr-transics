<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RefreshRentPlus implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;
    
    public $status;
    public $date;
    public $faultyContracts;
    
    /**
     * Create a new event instance.
     *
     * @param $status
     * @param $date
     */
    public function __construct($status, $date, $faultyContracts=[])
    {
        $this->status = $status;
        $this->date = $date;
        $this->faultyContracts = $faultyContracts;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('rentplus');
    }
    
}

<?php

namespace App\Events\Backend\Drivers;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class DriverUpdated
 * @package App\Events\Backend\Drivers
 */
class DriverUpdated extends Event
{
	use SerializesModels;

	/**
	 * @var $driver
	 */
	public $driver;

	/**
	 * @param $driver
	 */
	public function __construct($driver)
	{
		$this->driver = $driver;
	}
}
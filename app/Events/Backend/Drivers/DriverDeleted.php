<?php

namespace App\Events\Backend\Drivers;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class DriverCreated
 * @package App\Events\Backend\Drivers
 */
class DriverDeleted extends Event
{
	use SerializesModels;

	/**
	 * @var $driver
	 */
	public $driver;

	/**
	 * @param $driver
	 */
	public function __construct($driver)
	{
		$this->driver = $driver;
	}
}
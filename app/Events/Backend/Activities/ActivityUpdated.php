<?php

namespace App\Events\Backend\Activities;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class DriverUpdated
 * @package App\Events\Backend\Activities
 */
class ActivityUpdated extends Event
{
	use SerializesModels;

	/**
	 * @var $activity
	 */
	public $activity;

	/**
	 * @param $activity
	 */
	public function __construct($activity)
	{
		$this->activity = $activity;
	}
}
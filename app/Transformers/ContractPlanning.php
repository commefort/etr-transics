<?php

namespace App\Transformers;


use App\Models\Contract;
use League\Fractal\TransformerAbstract;

class ContractPlanning extends TransformerAbstract
{
    protected $date;
    protected $duplicateContracts;
    
    public function __construct($date, &$duplicateContracts)
    {
        $this->date = $date;
        $this->duplicateContracts = &$duplicateContracts;
    }
    
    
    public function transform(Contract $contract)
    {
        $type = null;
        if($contract->date_in->format('d/m/Y') == $this->date && $contract->date_out->format('d/m/Y') == $this->date)
        {
            if($contract->delivery && $contract->pickup)
            {
                $type = 'pickup';
            }
            else if($contract->delivery)
            {
                $type = 'delivery';
            }
            else if($contract->pickup)
            {
                $type = 'pickup';
            }
        }
        
        if($type == null)
        {
            // Decide on the type
            $type = ($contract->date_out->format('d/m/Y') == $this->date) ? 'delivery' : 'pickup';
        }
        
    
        // Make sure we're handling the delivery/pickup
        if(!$contract->{$type})
            return $this->null();
        
        // Keep track of cuplicate contracts
        if(!isset($this->duplicateContracts[$contract->order_number]))
        {
            $this->duplicateContracts[$contract->order_number] = [
                'pickup' => [],
                'delivery' => []
            ];
        }
    
        // Register contract
        $this->duplicateContracts[$contract->order_number][$type][] = $contract->id;
        
        // Transform contract
        return [
            'id' => $contract->id,
            'number' => $contract->order_number,
            'is_duplicate' => count($this->duplicateContracts[$contract->order_number][$type]) > 1,
            'duplicate_number' => count($this->duplicateContracts[$contract->order_number][$type]),
            'type' => $type,
            'status' => $contract->status,
            'infoWindow' => false,
            'state' => 'shown',
            'location' => [
                'name' => $contract->address_title,
                'street' => $contract->address_street,
                'postal' => $contract->address_location,
                'country' => $contract->address_country,
                'coordinates' => [
                    'lat' => (float) $contract->address_lat,
                    'lng' => (float) $contract->address_lng,
                ]
            ],
            'customer' => [
                'key' => $contract->customer->rentplus_key,
                'person' => $contract->customer_contact_name,
                'phone' => $contract->customer_contact_phone,
                'email' => ($contract->contact) ? $contract->contact->email : '',
            ],
            'contact' => [
                'person' => $contract->location_contact_name,
                'phone' => $contract->location_contact_phone
            ],
            'drivers' => [
                'delivery' => $contract->driver_in,
                'pickup' => $contract->driver_out
            ],
            'dates' => [
                'in' => $contract->date_in->format('d/m/y'),
                'out' => $contract->date_out->format('d/m/y')
            ],
            'show' => true,
            'slot' => false,
            'transics_status' => 'not_executed'
        ];
    }
}
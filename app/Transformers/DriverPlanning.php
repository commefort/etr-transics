<?php

namespace App\Transformers;


use App\Models\Driver;
use App\Models\DriverSchedule;
use League\Fractal\TransformerAbstract;

class DriverPlanning extends TransformerAbstract
{
    protected $date;
    
    public function __construct($date)
    {
        $this->date = $date;
    }
    
    public function transform(Driver $driver)
    {
        $data = [
            'id' => $driver->id,
            'planning' => [],
            'type' => $driver->type,
        ];
        
        foreach($driver->tasks as $schedule)
        {
            if(($schedule->type == 'task'))
            {
                $location = $schedule->meta['location'];
                $meta = [
                    'title' => $schedule->meta['title'],
                    'text' => $schedule->meta['text'],
                    'address' => $schedule->meta['address'],
                    'location' => $schedule->meta['location'],
                    'email' => isset($schedule->meta['email']) ? $schedule->meta['email'] : '',
                    'telephone' => isset($schedule->meta['telephone']) ? $schedule->meta['telephone'] : '',
                    'type' => $schedule->meta['type']
                ];
                $title = '';
            }
            else
            {
                $contract = $schedule->contract();
                
                // Make sure the contract is still planned on this day
                switch($schedule->activity_id)
                {
                    // Levering (out)
                    case 3;
                        $checkDate = $contract->date_out;
                    break;
                    // retour (in)
                    case 4:
                        $checkDate = $contract->date_in;
                    break;
                }
                
                // Doesn't seem to be
                if($schedule->date->format('d/m/y') != $checkDate->format('d/m/y'))
                {
                    return false;
                }
                
                $location = [
                    'coordinates' => [
                        'lat' => $contract->address_lat,
                        'lng' => $contract->address_lng
                    ]
                ];
                $meta = [
                    'number' => $contract->order_number,
                    'contract_id' => $contract->id,
                    'id' => $contract->id
                ];
                
                $title = $contract->address_title;
            }
            $plan = [
                'id' => (int)$schedule->id,
                'date' => $schedule->date->format('d-m-Y'),
                'slot' => $schedule->slot,
                'status' => $schedule->status,
                'label' => ($schedule->order == 0) ? '?' : $schedule->order,
                'order' => $schedule->order,
                'type' => $schedule->type,
                'activity' => (int)$schedule->activity->transics_id,
                'location' => $location,
                'meta' => $meta,
                'title' => $title,
                'transics_status' => $schedule->transics_status
            ];
    
            switch($schedule->type)
            {
                case 'contract':
                    $plan['contract_id'] = $schedule->meta['contract_id'];
                break;
                case 'task':
                    $plan['title'] = $schedule->meta['title'];
                    $plan['text'] = $schedule->meta['text'];
                break;
            }
            
            $data['planning'][] = $plan;
        }
        
        return $data;
    }
}
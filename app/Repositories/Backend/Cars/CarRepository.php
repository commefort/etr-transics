<?php

namespace App\Repositories\Backend\Cars;

use App\Events\Backend\Drivers\DriverCreated;
use App\Events\Backend\Drivers\DriverDeleted;
use App\Events\Backend\Drivers\DriverUpdated;
use App\Models\Access\User\User;
use App\Models\Driver;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use App\Events\Backend\Access\User\UserCreated;
use App\Events\Backend\Access\User\UserUpdated;
use App\Events\Backend\Access\User\UserDeleted;
use App\Events\Backend\Access\User\UserRestored;
use App\Events\Backend\Access\User\UserDeactivated;
use App\Events\Backend\Access\User\UserReactivated;
use App\Events\Backend\Access\User\UserPasswordChanged;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Events\Backend\Access\User\UserPermanentlyDeleted;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;

/**
 * Class UserRepository
 * @package App\Repositories\User
 */
class CarRepository extends Repository
{
    /**
     * Associated Repository Model
     */
    const MODEL = Driver::class;
    
    /**
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
                               ->select([
                                   'drivers.id',
                                   'drivers.type',
                                   'drivers.plate',
                                   'drivers.status',
                                   'drivers.created_at',
                                   'drivers.updated_at',
                               ]);
        return $dataTableQuery;
    }
    
    /**
     * @param Model $input
     */
    public function create($input)
    {
        $data = $input['data'];
        
        $driver = $this->createDriverStub($data);
        
        DB::transaction(function () use ($driver, $data)
        {
            if (parent::save($driver))
            {
                
                event(new DriverCreated($driver));
                
                return true;
            }
            
            throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
        });
    }
    
    /**
     * @param Model $driver
     * @param array $input
     */
    public function update(Model $driver, array $input)
    {
        $data = $input['data'];
        
        DB::transaction(function () use ($driver, $data)
        {
            if (parent::update($driver, $data))
            {
                event(new DriverUpdated($driver));
                
                return true;
            }
            
            throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
        });
    }
    
    /**
     * @param Model $driver
     *
     * @return bool
     * @throws GeneralException
     */
    public function delete(Model $driver)
    {
        if (access()->id() == $driver->id)
        {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_self'));
        }
        
        if (parent::delete($driver))
        {
            event(new DriverDeleted($driver));
            
            return true;
        }
        
        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }
    
    /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createDriverStub($input)
    {
        $driver = self::MODEL;
        $driver = new $driver;
        $driver->type = 'external';
        $driver->fleet = 'external';
        $driver->plate = $input['plate'];
        $driver->status = $input['status'];
        $driver->vehicle_id = 0;
        $driver->transics_id = 0;
        $driver->current_kms = 0;
        
        return $driver;
    }
}

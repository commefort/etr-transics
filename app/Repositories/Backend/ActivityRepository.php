<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Activities\ActivityUpdated;
use App\Models\TransicsActivity;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ActivityRepository
 * @package App\Repositories\User
 */
class ActivityRepository extends Repository
{
    /**
     * Associated Repository Model
     */
    const MODEL = TransicsActivity::class;
    
    /**
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
                               ->select([
                                   'transics_activities.id',
                                   'transics_activities.name',
                                   'transics_activities.type',
                                   'transics_activities.color',
                                   'transics_activities.is_planning',
                                   'transics_activities.created_at',
                                   'transics_activities.updated_at',
                               ]);
        return $dataTableQuery;
    }
    
    /**
     * @param Model $activity
     * @param array $input
     */
    public function update(Model $activity, array $input)
    {
        $data = $input['data'];
        
        DB::transaction(function () use ($activity, $data)
        {
            if (parent::update($activity, $data))
            {
                event(new ActivityUpdated($activity));
                
                return true;
            }
            
            throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
        });
    }
    
    /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createDriverStub($input)
    {
        $activity = self::MODEL;
        $activity = new $activity;
        $activity->color = $input['color'];
        dd($input);
        
        return $activity;
    }
}

<?php

namespace App\Console;

use App\Console\Commands\ClearPlanning;
use App\Console\Commands\ImportRentPlus;
use App\Console\Commands\ImportTransics;
use App\Console\Commands\NotifivyPlivo;
use App\Console\Commands\NotifyExternalDrivers;
use App\Console\Commands\TrackTaskStatus;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ImportRentPlus::class,
        ImportTransics::class,
        ClearPlanning::class,
        NotifyExternalDrivers::class,
        TrackTaskStatus::class,
        NotifivyPlivo::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Import rent+ tasks
        $schedule->command('import:rent+')->everyThirtyMinutes();
        
        // Import transics data
        $schedule->command('import:transics')->hourly();
        
        // Send out notifications ofr external drivers
        $schedule->command('notify:external')->dailyAt('19:00');
    
        // Track tasks' status
        $schedule->command('track:transics')->everyFiveMinutes();
        $schedule->command('track:transics')->dailyAt('23:58');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Contract;
use App\Models\ContractsDetail;
use App\Models\CustomersContact;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\TransicsActivity;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;

class ImportTransics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:transics';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports drivers and syncs schedules from/with transics';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client([
            'headers' => [
                'Content-Type' => 'text/xml',
            ],
        ]);
        
        $this->activities($client);
        $this->vehicles($client);
        
    }
    
    protected function vehicles($client)
    {
        $xml = '<VehicleSelection></VehicleSelection>';
        
        $vehicles = $this->doRequest($client, 'Get_Vehicles_V10', $xml);
        
        if(count($vehicles['Errors']) > 0)
        {
            $this->warn(var_dump($vehicles['Errors']));
            return;
        }
        
        if(count($vehicles['Vehicles']['InterfaceVehicleResult_V10']) > 0)
        {
            foreach($vehicles['Vehicles']['InterfaceVehicleResult_V10'] as $key => $vehicle)
            {
                Driver::updateOrCreate([
                    'transics_id' => $vehicle['VehicleTransicsID']
                ], [
                    'plate' => $vehicle['LicensePlate'],
                    'fleet' => $vehicle['VehicleFleetNumber'],
                    'vehicle_id' => $vehicle['VehicleID'],
                    'current_kms' => is_array($vehicle['CurrentKms']) ? 0 : $vehicle['CurrentKms'],
                    'status' => $vehicle['Maintenance'] > 0 ? 'maintenance' : ($vehicle['Inactive'] === true || $vehicle['Inactive'] == 'true') ? 'inactive' : 'operational'
                ]);
            }
        }
        
        $this->info('Vehicles imported');
    }
    
    protected function activities($client)
    {
        $xml = '<ActivitySelection></ActivitySelection>';
        
        $activities = $this->doRequest($client, 'Get_ActivityList_V2', $xml);
        
        if(count($activities['Errors']) > 0)
        {
            $this->warn(var_dump($activities['Errors']));
            return;
        }
        
        if(count($activities['Activities']['ActivityVersionResult_V5'][0]['Activities']['ActivityInfo_V5']) > 0)
        {
            foreach($activities['Activities']['ActivityVersionResult_V5'][0]['Activities']['ActivityInfo_V5'] as $key => $activity)
            {
                TransicsActivity::updateOrCreate([
                    'transics_id' => $activity['ID']
                ], [
                    'name' => $activity['Name'],
                    'type' => $activity['ActivityType'],
                    'is_planning' => ($activity['IsPlanning'] === true || $activity['IsPlanning'] == 'true')
                ]);
            }
        }
        
        $this->info('Activities imported');
    }
    
    protected function doRequest($client, $endPoint, $body, $dateTime=false)
    {
        if($dateTime == false)
        {
            $dateTime = '0001-01-01T00:00:00';
        }
        
        $xml = '<?xml version="1.0" encoding="utf-16"?>
                <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                  <soap:Body>
                    <' . $endPoint . ' xmlns="http://transics.org">
                      <Login>
                        <DateTime>'.$dateTime.'</DateTime>
                        <Version>0</Version>
                        <Dispatcher>'.env('TRANSICS_DISPATCHER').'</Dispatcher>
                        <Password>'.env('TRANSICS_PASSWORD').'</Password>
                        <SystemNr>'.env('TRANSICS_SYSTEMNR').'</SystemNr>
                        <Integrator>'.env('TRANSICS_DISPATCHER').'</Integrator>
                        <Language>NL</Language>
                      </Login>
                      ' . $body . '
                    </' . $endPoint . '>
                  </soap:Body>
                </soap:Envelope>';
        
        $responseToken = $endPoint . 'Response';
        $resultToken = $endPoint . 'Result';
        
        return $this->normaliseBody(
            $client->post(
                'https://tx-tango.tx-connect.com/IWS_ASMX/Service.asmx?WSDL',
                [
                    'body' => $xml,
                    'headers' => [
                        'Content-Type' => 'text/xml',
                        'SOAPACTION' => 'http://transics.org/' . $endPoint,
                    ],
                ])
                ->getBody()
                ->getContents()
        )[$responseToken][$resultToken];
    }
    
    protected function normaliseBody($body)
    {
        return json_decode(json_encode(simplexml_load_string(
            str_replace(['soap:', 'SOAP:'], '', $body),
            'SimpleXMLElement',
            LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NSCLEAN
        )), true)['Body'];
    }
    
}

<?php

namespace App\Console\Commands;

use App\Models\Contract;
use App\Models\ContractsDetail;
use App\Models\CustomersContact;
use App\Models\Customer;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;

class ImportRentPlus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:rent+';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports customers & orders from rent+';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client();
        
        $this->customers($client);
        $this->orders($client);
    }
    
    protected function customers($client)
    {
        $customers = $this->normaliseBody($client->get('http://mail.eurotaprent.be/RentPlusSetup/RentPlusServices.asmx/GetCustomerList?from=')->getBody()->getContents())->children()[0];
        Customer::unguard();
        
        $progress = new ProgressBar($this->output, $customers->count());
        $progress->setFormat(" %message%\n %current%/%max% [%bar%] %percent:3s%% %elapsed:6s% %memory:6s%");
        $progress->setMessage('Importing customers');
        $progress->start();
        
        
        foreach ($customers as $customer)
        {
            $progress->setMessage('Importing ' . $customer->Customer_Key);
            
            
            $model = Customer::firstOrCreate([
                'rentplus_key' => $customer->Customer_Key,
                'venture_number' => $customer->Venture_Number,
                'warehouse_number' => $customer->WareHouseNumber,
            ]);
    
    
            $this->getCustomerInfo($client, $customer, $model);
            
            $this->importContacts($client, $model, $progress);
            
            $progress->advance();
        }
        
        $progress->setMessage('Customers imported');
        $progress->finish();
    }
    
    /**
     *
     * @param $client
     * @param $customer
     *
     * @return array
     */
    protected function getCustomerInfo($client, $customer, $model)
    {
        $this->info('http://mail.eurotaprent.be/RentPlusSetup/RentPlusServices.asmx/GetCustomerInfo?customerKey='.$customer->Customer_Key.'&Venture_Number='.$this->ifEmpty($customer->Venture_Number).'&WarehouseNumber='.$this->ifEmpty($customer->WareHouseNumber));
        
        try {
            $info = $this->normaliseBody(
                $client->get('http://mail.eurotaprent.be/RentPlusSetup/RentPlusServices.asmx/GetCustomerInfo?customerKey='.$customer->Customer_Key.'&Venture_Number='.$this->ifEmpty($customer->Venture_Number).'&WarehouseNumber='.$this->ifEmpty($customer->WareHouseNumber))
                       ->getBody()
                       ->getContents()
            )->children()[0];
        }
        catch(\Exception $e)
        {
            return;
        }
        
    
        $data = [];
        
        if(!is_object($info))
        {
            return;
        }
        
        if($info->CustomerInfo->Mobile_Phone != '')
            $model->mobile = $info->CustomerInfo->Mobile_Phone;
        if($info->CustomerInfo->Email != '')
            $model->email = $info->CustomerInfo->Email;
        if($info->CustomerInfo->Language != '')
            $model->language = $info->CustomerInfo->Language;
        
        if($info->CustomerInfo->Address_Line_1 != '')
        {
            $model->address = $info->CustomerInfo->Address_Line_1 . ' ' . $info->CustomerInfo->Address_Line_2;
            $model->postal = $info->CustomerInfo->Postal_Code . ' ' . $info->CustomerInfo->City;
        }
    
        $model->save();
        return $data;
    }
    
    protected function importContacts($client, $customer, $progress)
    {
        $contacts = $this->normaliseBody($client->get('http://mail.eurotaprent.be/RentPlusSetup/RentPlusServices.asmx/GetContactInfo?customerKey='.urlencode($customer->rentplus_key))->getBody()->getContents())->children()[0];
        
        if($contacts == null)
        {
            return;
        }
        
        $totalContacts  = count($contacts);
        $progress->setMessage('Importing ' . $totalContacts . ' contacts');
        
        foreach($contacts->children() as $contact)
        {
            CustomersContact::updateOrCreate([
                'rentplus_key' => $contact->Contact_Key
            ], [
                'customer_id' => $customer->id,
                'name' => $contact->Name . ' ' . $contact->First_Name,
                'email' => $contact->E_Mail,
                'phone' => $contact->Mobile_Phone_Number
            ]);
        }
    }
    
    protected function orders($client)
    {
        $customers = Customer::all();
        
        Contract::unguard();
        ContractsDetail::unguard();
        
        $this->info('Importing customer\'s orders');
    
        $progress = new ProgressBar($this->output, $customers->count());
        $progress->setFormat(" %message%\n %current%/%max% [%bar%] %percent:3s%% %elapsed:6s% %memory:6s%");
        $progress->start();
        
        foreach ($customers as $customer)
        {
            $progress->setMessage('Importing for ' . $customer->rentplus_key);
            
            $orders = $this->normaliseBody(
                $client->get('http://mail.eurotaprent.be/RentPlusSetup/RentPlusServices.asmx/GetContracts?customerKey=' . urlencode($customer->rentplus_key) . '&Venture_Number=')
                       ->getBody()
                       ->getContents()
            )->children()[0];
            
            if($orders == null)
            {
                $progress->setMessage('No contracts for ' . $customer->rentplus_key);
                $progress->advance();
                continue;
            }
            $orderCount = count($orders);
            $i = 0;
            
            foreach ($orders->children() as $order)
            {
                // @todo needed as long as customerDetail endpoint does not work
                if ($i == $orderCount - 1)
                {
                    $cData = [
                        'name' => $order->Customer_Name_1,
                        'address' => $order->Customer_Address_Line_1,
                        'postal' => $order->Customer_Postal_code . ' ' . $order->Customer_City,
                    ];
                    
                    if (!empty( $order->Customer_Telephone ))
                    {
                        $cData['mobile'] = $order->Customer_Telephone;
                    }
                    
                    $customer->forceFill($cData);
                    $customer->save();
                }
                
                //dd($order->Contact_Mobile_Phone_Number, $order->Location_Contact_Mobile_Phone_Number[0]);
                
                // Create or update the contract
                $contract = Contract::updateOrCreate([
                    'order_number' => $order->Order_Number,
                    'customer_id' => $customer->id
                ], [
                    'address_title' => $order->Order_Customer_Reference,
                    'address_street' => $order->Delivery_Street_Nr,
                    'address_location' => $order->Delivery_Postal_Code . ' ' . $order->Delivery_City,
                    'address_country' => $order->Delivery_Country,
                    'contact_id' => $order->Contact_Key,
                    'customer_contact_name' => $order->Contact_Name . ' ' . $order->Contact_First_Name,
                    'customer_contact_phone' => empty($order->Contact_Mobile_Phone_Number) ? null : $order->Contact_Mobile_Phone_Number[0],
                    'location_contact_name' => $order->Location_Contact_Name . ' ' . $order->Location_Contact_First_Name,
                    'location_contact_phone' => empty($order->Location_Contact_Mobile_Phone_Number) ? null : $order->Location_Contact_Mobile_Phone_Number[0],
                    'price' => $order->Price_Sales || 0.0,
                    'delivery' => (bool) $order->Delivery_Done_By_Us,
                    'pickup' => (bool) $order->Recall_Done_By_Us,
                    'date_in' => Carbon::createFromFormat(Carbon::ATOM, $order->Date_Transport_In),
                    'date_out' => Carbon::createFromFormat(Carbon::ATOM, $order->Date_Transport_Out)
                ]);
                
                $this->getContractCoordinates($contract, $progress, $client);
                
                // Register/reindex order details if date happens today or in the future (should've been updated last on yesterday)
                if(($contract->date_in >= Carbon::now() && $contract->updated_at < Carbon::now()->subDay()) || $contract->updated_at == $contract->created_at)
                {
                    $orderDetails = $this->normaliseBody(
                        $client->get('http://mail.eurotaprent.be/RentPlusSetup/RentPlusServices.asmx/GetOrderDetail?orderNumber=' . $order->Order_Number . '&ExternalOrderNumber=')
                               ->getBody()
                               ->getContents()
                    )->children()[0];
                    
                    // We're reindexing, remove all existing articles
                    $contract->details()->delete();
                    
                    // Loop over the articles
                    foreach($orderDetails->OrderLine as $article)
                    {
                        // Add the article
                        ContractsDetail::create([
                            'contract_id' => $contract->id,
                            'rentplus_id' => $article->_Item_id,
                            'name' => $article->Article_Name,
                            'article_key' => $article->Article_Key,
                            'module_number' => $article->ModuleNumber,
                            'comment' => $article->Comment || null,
                            'type' => $article->Article_Type,
                            'name_translated' => $article->Article_Name_Translated,
                            'amount' => $article->Number,
                            'sign' => $article->Sign,
                            'price' => $article->Price_Sales,
                        ]);
                    }
                }
                
                $i++;
                $progress->setMessage('Processed ' . $i .'/' . $orderCount. ' orders for ' . $customer->rentplus_key);
            }
    
            $progress->advance();
        }
    
        $progress->finish();
        $this->comment('');
        $this->comment('Imported orders');
    }
    
    protected function getContractCoordinates(Contract $contract, $progress, $client)
    {
        if(empty($contract->address_lng))
        {
            $progress->setMessage('Retrieving coordinates for contract location');
            
            $response = \GuzzleHttp\json_decode($client->get('https://maps.googleapis.com/maps/api/geocode/json?language=nl&address=' . urlencode($contract->address_street . ' ' . $contract->address_postal . ' ' . $contract->address_country) . '&key='.env('GMAPS_KEY'))->getBody()->getContents());
            
            if(count($response->results) > 0)
            {
                $contract->forceFill([
                    'address_lng' => $response->results[0]->geometry->location->lng,
                    'address_lat' => $response->results[0]->geometry->location->lat,
                ]);
                $contract->save();
                
                $progress->setMessage('Coordinates were set');
                return;
            }
            
            $progress->setMessage('Coordinates not found');
        }
    }
    
    protected function normaliseBody($body)
    {
        $replace = [
            ' xmlns="www.rentplus.be/webservices/"',
            ' xmlns:mstns="http://tempuri.org/dsCustomerList.xsd" xmlns="http://tempuri.org/dsCustomerList.xsd"',
            ' xmlns:mstns="www.rentplus.be/webservices/CustomerInfo.xsd" xmlns="www.rentplus.be/webservices/CustomerInfo.xsd"',
            ' xmlns:mstns="www.rentplus.be/webservices/ArticleList.xsd"',
            ' xmlns="www.rentplus.be/webservices/ArticleList.xsd"',
            ' xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata"',
            ' targetNamespace="http://tempuri.org/dsCustomerList.xsd"',
            ' targetNamespace="www.rentplus.be/webservices/ArticleList.xsd"',
            ' targetNamespace="www.rentplus.be/webservices/CustomerInfo.xsd"',
            ' xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1"',
            ' xmlns="http://tempuri.org/dsCustomerList.xsd"',
            ' xmlns="www.rentplus.be/webservices/ArticleInfo.xsd"',
            ' xmlns="www.rentplus.be/webservices/Order.xsd"',
            ' xmlns="www.rentplus.be/webservices/CustomerInfo.xsd"',
        ];
        
        return simplexml_load_string(str_replace($replace, '', $body), 'SimpleXMLElement',
            LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NSCLEAN)->children()[1];
    }
    
    protected function ifEmpty($val)
    {
        return ( empty( $val ) ) ? "''" : $val;
    }
}

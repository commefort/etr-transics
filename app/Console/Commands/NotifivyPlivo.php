<?php

namespace App\Console\Commands;

use App\Models\DriverSchedule;
use App\Notifications\NotifyPlannedContract;
use Illuminate\Console\Command;

class NotifivyPlivo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:plivo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Notification::send(DriverSchedule::find(230), new NotifyPlannedContract());
    }
}

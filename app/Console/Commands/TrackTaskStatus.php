<?php

namespace App\Console\Commands;

use App\Models\DriverSchedule;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class TrackTaskStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'track:transics';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks today\'s tasks for their status';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client([
            'headers' => [
                'Content-Type' => 'text/xml',
            ],
        ]);
        
        $this->planning($client);
        
    }
    
    public function planning($client)
    {
        // Load all tasks that should still be executed today (or even yesterday)
        DriverSchedule::where('transics_status', '!=', 'finished')
                      ->where(function ($q)
                      {
                          $q->whereDate('date', '=', Carbon::today()->toDateTimeString())
                            ->orWhereDate('date', '=', Carbon::yesterday()->toDateTimeString());
                      })
                      ->get()
                      ->each(function ($task) use ($client)
                      {
                          $this->task($client, $task);
                      });
    }
    
    protected function task($client, $task)
    {
        // Request body (request info about a place)
        $xml = '<PlanningSelection>
                <Item>
                <ID>' . $task->place_id . '</ID>
                <PlanningSelectionType>PLACE</PlanningSelectionType>
                </Item>
                </PlanningSelection>';
        
        
        try
        {
            $items = $this->doRequest($client, 'Get_Planning', $xml);
            
            if (count($items['Errors']) > 0)
            {
                $this->warn(var_dump($items['Errors'], $xml));
                
                return;
            }
            
            if (isset( $items['ItemSelection']['Places']['PlaceItemResult'] ))
            {
                $status = $items['ItemSelection']['Places']['PlaceItemResult']['Status'];
                $task->transics_status = strtolower($status);
                $task->save();
            }
            
            $this->info('Task checked: ' . $task->id . ' - ' . $task->place_id);
        } catch (\Exception $e)
        {
            // There was an error, possibly the place_id does nto exists in transics
            // fail silently
        }
    }
    
    protected function doRequest($client, $endPoint, $body, $dateTime = false)
    {
        if ($dateTime == false)
        {
            $dateTime = '0001-01-01T00:00:00';
        }
        
        $xml = '<?xml version="1.0" encoding="utf-16"?>
                <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                  <soap:Body>
                    <' . $endPoint . ' xmlns="http://transics.org">
                      <Login>
                        <DateTime>' . $dateTime . '</DateTime>
                        <Version>0</Version>
                        <Dispatcher>' . env('TRANSICS_DISPATCHER') . '</Dispatcher>
                        <Password>' . env('TRANSICS_PASSWORD') . '</Password>
                        <SystemNr>' . env('TRANSICS_SYSTEMNR') . '</SystemNr>
                        <Integrator>' . env('TRANSICS_DISPATCHER') . '</Integrator>
                        <Language>NL</Language>
                      </Login>
                      ' . $body . '
                    </' . $endPoint . '>
                  </soap:Body>
                </soap:Envelope>';
        
        $responseToken = $endPoint . 'Response';
        $resultToken = $endPoint . 'Result';
        
        return $this->normaliseBody(
            $client->post(
                'https://tx-tango.tx-connect.com/IWS_ASMX/Service.asmx?WSDL',
                [
                    'body' => $xml,
                    'headers' => [
                        'Content-Type' => 'text/xml',
                        'SOAPACTION' => 'http://transics.org/' . $endPoint,
                    ],
                ])
                   ->getBody()
                   ->getContents()
        )[$responseToken][$resultToken];
    }
    
    protected function normaliseBody($body)
    {
        return json_decode(json_encode(simplexml_load_string(
            str_replace(['soap:', 'SOAP:'], '', $body),
            'SimpleXMLElement',
            LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NSCLEAN
        )), true)['Body'];
    }
    
}

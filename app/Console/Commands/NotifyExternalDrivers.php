<?php

namespace App\Console\Commands;

use App\Models\Driver;
use App\Models\DriverSchedule;
use App\Notifications\ExternalPlanningNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;

class NotifyExternalDrivers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:external';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A cron that notifies external drivers of their planning.';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tomorrow = Carbon::tomorrow();
        
        $externalDriverIds = Driver::where('type', 'external')->where('email', '!=', '')->select('id')->pluck('id');
        
        if (count($externalDriverIds) == 0)
        {
            $this->warn('No drivers found with an email address');
            
            return;
        }
        
        $tasks = DriverSchedule::with('driver')
                               ->whereDate('date', '=', $tomorrow->format('Y-m-d'))
                               ->whereIn('driver_id', $externalDriverIds)
                               ->get()
                               ->groupBy('driver_id')
                               ->map(function ($schedule)
                               {
                                   return $schedule->groupBy('slot')
                                                   ->map(function ($items)
                                                   {
                                                       return $items->sortBy('order')->map(function (
                                                           DriverSchedule $task
                                                       ) {
                                                           if ($task->type == 'contract')
                                                           {
                                                               $contract = $task->contract();
                        
                                                               return [
                                                                   'title' => 'Bon nr. ' . $contract->order_number . ' - ' . $contract->address_title,
                                                                   'address' => $contract->address_street . ' ' . $contract->address_location,
                                                                   'contact' => [
                                                                       'name' => $contract->location_contact_name,
                                                                       'phone' => $contract->location_contact_phone,
                                                                   ],
                                                                   'type' => $task->activity->name,
                                                               ];
                                                           }
                                                           else
                                                           {
                                                               return [
                                                                   'title' => $task->meta['title'] . ' - ' . $task->meta['text'],
                                                                   'address' => $task->meta['address'],
                                                                   'contact' => [
                                                                       'name' => '',
                                                                       'phone' => $task->meta['telephone'],
                                                                   ],
                                                                   'type' => $task->activity->name,
                                                               ];
                                                           }
                    
                                                       });
                                                   });
                               });
    
        echo count($tasks);
        if(count($tasks) > 0)
        {
            foreach ($tasks as $driver_id => $tasks)
            {
                $driver = Driver::find($driver_id);
                \Notification::send($driver, new ExternalPlanningNotification($tasks, $driver->plate));
            }
        }
    }
}

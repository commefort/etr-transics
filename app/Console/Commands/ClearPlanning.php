<?php

namespace App\Console\Commands;

use App\Models\Contract;
use App\Models\ContractsDetail;
use App\Models\CustomersContact;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\TransicsActivity;
use Carbon\Carbon;
use DateTime;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use SoapClient;
use stdClass;
use Symfony\Component\Console\Helper\ProgressBar;

class ClearPlanning extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:planning';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear the demo driver\'s planning';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $options = [
            'cache_wsdl' => 0,
        ];
        
        /* Create Soap client */
        $client = new SoapClient('https://tx-tango.tx-connect.com/IWS_ASMX/Service.asmx?WSDL', $options);
        
        /* Create login object */
        $login = new stdClass();
        $login->DateTime = date(DateTime::W3C);
        $login->Dispatcher = env('TRANSICS_DISPATCHER');
        $login->Password = env('TRANSICS_PASSWORD');
        $login->SystemNr = env('TRANSICS_SYSTEMNR');
        $login->ApplicationName = 'EuroTapRent planner';
        $login->ApplicationVersion = '1.0';
        $login->PcName = 'COMMEFORT-DO';
        $login->Integrator = env('TRANSICS_INTEGRATOR');
        $login->Language = 'NL';
        $login->Version = '0';
        
        $payload = new \stdClass();
        $payload->Login = $login;
        $payload->PlanningSelection = (object) [
            'Historical' => [
                'SelectionType' => 'PLANNING',
                'DateTimeSelectionType' => 'STARTED',
                'DateTimeRange' => [
                    'StartDate' => Carbon::create(2016, 12, 20)->toRfc3339String(),
                    'EndDate' => Carbon::create(2017, 01, 20)->toRfc3339String(),
                ],
                'Vehicle' => [
                    'IdentifierVehicleType' => 'LICENSE_PLATE',
                    'Id' => 'Training unit',
                ],
            ],
            'Options' => [
                'IncludeModificationsOnly' => false,
                'IncludePlaceScanIDs' => false,
                'IncludeQuestionPathInfo' => false,
                'IncludeTachoActivities' => false,
                'IncludeTripRegistrations' => false
            ]
        ];
        
        //dd($payload);
        $response = $client->Get_Planning_V6($payload);
        
        $result = $response->Get_Planning_V6Result->HistoricalSelection->Plannings->PlanningInterfaceResult_V5->Places->PlaceInterfaceResult_V4;
      
        foreach($result as $planned)
        {
            if($planned->Status == 'CANCELED')
                continue;
            
            $payload->PlanningItemSelection = (object) [
                'PlanningSelectionType' => 'PLACE',
                'ID' => $planned->PlaceId,
            ];
            $lastResponse = $client->Cancel_Planning_V2($payload);
        }
    }
    
    protected function vehicles($client)
    {
        $xml = '<VehicleSelection></VehicleSelection>';
        
        $vehicles = $this->doRequest($client, 'Get_Vehicles_V10', $xml);
        
        if (count($vehicles['Errors']) > 0)
        {
            $this->warn(var_dump($vehicles['Errors']));
            
            return;
        }
        
        if (count($vehicles['Vehicles']['InterfaceVehicleResult_V10']) > 0)
        {
            foreach ($vehicles['Vehicles']['InterfaceVehicleResult_V10'] as $key => $vehicle)
            {
                Driver::updateOrCreate([
                    'transics_id' => $vehicle['VehicleTransicsID'],
                ], [
                    'plate' => $vehicle['LicensePlate'],
                    'fleet' => $vehicle['VehicleFleetNumber'],
                    'vehicle_id' => $vehicle['VehicleID'],
                    'current_kms' => is_array($vehicle['CurrentKms']) ? 0 : $vehicle['CurrentKms'],
                    'status' => $vehicle['Maintenance'] > 0 ? 'maintenance' : ( $vehicle['Inactive'] === true || $vehicle['Inactive'] == 'true' ) ? 'inactive' : 'operational',
                ]);
            }
        }
        
        $this->info('Vehicles imported');
    }
    
    protected function activities($client)
    {
        $xml = '<ActivitySelection></ActivitySelection>';
        
        $activities = $this->doRequest($client, 'Get_ActivityList_V2', $xml);
        
        if (count($activities['Errors']) > 0)
        {
            $this->warn(var_dump($activities['Errors']));
            
            return;
        }
        
        if (count($activities['Activities']['ActivityVersionResult_V5'][0]['Activities']['ActivityInfo_V5']) > 0)
        {
            foreach ($activities['Activities']['ActivityVersionResult_V5'][0]['Activities']['ActivityInfo_V5'] as $key => $activity)
            {
                TransicsActivity::updateOrCreate([
                    'transics_id' => $activity['ID'],
                ], [
                    'name' => $activity['Name'],
                    'type' => $activity['ActivityType'],
                    'is_planning' => ( $activity['IsPlanning'] === true || $activity['IsPlanning'] == 'true' ),
                ]);
            }
        }
        
        $this->info('Activities imported');
    }
    
    protected function doRequest($client, $endPoint, $body, $dateTime = false)
    {
        if ($dateTime == false)
        {
            $dateTime = '0001-01-01T00:00:00';
        }
        
        $xml = '<?xml version="1.0" encoding="utf-16"?>
                <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                  <soap:Body>
                    <' . $endPoint . ' xmlns="http://transics.org">
                      <Login>
                        <DateTime>' . $dateTime . '</DateTime>
                        <Version>0</Version>
                        <Dispatcher>COMMEFORT</Dispatcher>
                        <Password>COMMEFORT_2502000992</Password>
                        <SystemNr>992</SystemNr>
                        <Integrator>COMMEFORT</Integrator>
                        <Language>NL</Language>
                      </Login>
                      ' . $body . '
                    </' . $endPoint . '>
                  </soap:Body>
                </soap:Envelope>';
        
        $responseToken = $endPoint . 'Response';
        $resultToken = $endPoint . 'Result';
        
        return $this->normaliseBody(
            $client->post(
                'https://tx-tango.tx-connect.com/IWS_ASMX/Service.asmx?WSDL',
                [
                    'body' => $xml,
                    'headers' => [
                        'Content-Type' => 'text/xml',
                        'SOAPACTION' => 'http://transics.org/' . $endPoint,
                    ],
                ])
                   ->getBody()
                   ->getContents()
        )[$responseToken][$resultToken];
    }
    
    protected function normaliseBody($body)
    {
        return json_decode(json_encode(simplexml_load_string(
            str_replace(['soap:', 'SOAP:'], '', $body),
            'SimpleXMLElement',
            LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NSCLEAN
        )), true)['Body'];
    }
    
}


<?php
if(!isset($_POST["btnsubmit"])) {
    ?>
    <form style="width:600px;" method="post" action="Get_Vehicle.php">
        <fieldset>
            <legend>Connection</legend>
            <label>WSDL</label>
            <input type="text" name="WSDL" placeholder="WSDL" style="width: 100%;">
            <p>
            <table>
                <tr>
                    <td><label>Dispatcher</label></td>
                    <td><input type="text" name="Dispatcher" placeholder="Dispatcher"></td>
                    <td><label>System number</label></td>
                    <td><input type="text" name="Sysnr" placeholder="System number"></td>
                </tr>
                <tr>
                    <td><label>Password</label></td>
                    <td><input type="text" name="Password" placeholder="Password"></td>
                    <td><label>Integrator</label></td>
                    <td><input type="text" name="Integrator" placeholder="Integrator"></td>
                </tr>
                <tr>
                    <td><label>Specific vehicle</label></td>
                    <td><input type="text" name="Vehicle" placeholder="OPTIONAL"></td>
                    <td><input type="submit" name="btnsubmit" value="Get Vehicles"></td>
                </tr>
            </table>
            </p>
        </fieldset>
    </form>
    <?php
} else {
    ?>
    <form style="width:600px;" method="post" action="Get_Vehicle.php">
        <fieldset>
            <legend>Connection</legend>
            <label>WSDL</label>
            <input type="text" name="WSDL" value="<?php echo $_POST['WSDL']?>" style="width: 100%;">
            <p>
            <table>
                <tr>
                    <td><label>Dispatcher</label></td>
                    <td><input type="text" name="Dispatcher" value="<?php echo $_POST['Dispatcher']?>"></td>
                    <td><label>System number</label></td>
                    <td><input type="text" name="Sysnr" value="<?php echo $_POST['Sysnr']?>"></td>
                </tr>
                <tr>
                    <td><label>Password</label></td>
                    <td><input type="text" name="Password" value="<?php echo $_POST['Password']?>"></td>
                    <td><label>Integrator</label></td>
                    <td><input type="text" name="Integrator" value="<?php echo $_POST['Integrator']?>"></td>
                </tr>
                <tr>
                    <td><label>Specific vehicle</label></td>
                    <td><input type="text" name="Vehicle" value="<?php echo $_POST['Vehicle']?>"></td>
                    <td><input type="submit" name="btnsubmit" value="Get Vehicles"></td>
                </tr>
            </table>
            </p>
        </fieldset>
    </form>
    <?php
}
?>

<?php
date_default_timezone_set('Europe/Brussels');//or change to whatever timezone you want

if(isset($_POST["btnsubmit"])) {
    //Get data from form
    $dispatcher = $_POST["Dispatcher"];
    $password = $_POST["Password"];
    $sysnr = $_POST["Sysnr"];
    $integrator = $_POST["Integrator"];
    $wsdl = $_POST["WSDL"];
    if(!empty($_POST["Vehicle"])) {
        $vehicle = $_POST["Vehicle"];
    }
    
    $options = array(
        'cache_wsdl' => 0,
    );
    
    /* Create Soap client */
    $client = new SoapClient($wsdl, $options);
    
    /* Create login object */
    $login = new stdClass();
    $login->DateTime = date(DateTime::W3C);
    $login->Dispatcher = $dispatcher;
    $login->Password = $password;
    $login->SystemNr = intval($sysnr);
    $login->ApplicationName = 'PHP - Example IWS2.0';
    $login->ApplicationVersion = '1.0';
    $login->PcName = 'PHP-SERVER01';
    $login->Integrator = $integrator;
    $login->Language = 'EN';
    $login->Version = '0';
    
    /*Create VehicleSelection object*/
    $vehsel = new stdClass();
    if(empty($vehicle) == false){
        $identifiers = new stdClass();
        $identifiers->IdentifierVehicleType = "ID";
        $identifiers->Id = $vehicle;
        $vehsel->Identifiers[] = $identifiers;
    }
    
    /* Create global sender object */
    $sender = new stdClass();
    $sender->Login = $login;
    $sender->VehicleSelection = $vehsel;
    
    
    if(isset($client->Get_Vehicles($sender)->Get_VehiclesResult->Vehicles)) {
        /* Call the webservice */
        $return = $client->Get_Vehicles($sender)->Get_VehiclesResult->Vehicles;
        
        /* Prints out the response object */
        $count = count($return->VehicleResult);
        echo "<b>VEHICLES:</b> <br>";
        
        if($count > 1) {
            for ($i = 0; $i < $count; $i++) {
                print("Vehicle: " . $return->VehicleResult[$i]->VehicleID . " with " . $return->VehicleResult[$i]->CurrentKms . " kilometers" . "<br>");
            }
        } else {
            print("Vehicle: " . $return->VehicleResult->VehicleID . " with " . $return->VehicleResult->CurrentKms . " kilometers");
        }
    } else {
        //print error
        $return = $client->Get_Vehicles($sender)->Get_VehiclesResult->Errors;
        print "Error: " . $return->Error->ErrorCode;
    }
} else {
    echo "<b>ENTER DATA</b>";
}
?>


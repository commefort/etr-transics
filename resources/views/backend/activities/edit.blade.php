@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.activities.management') . ' | ' . trans('labels.backend.activities.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.activities.header') }}
        <small>{{ trans('labels.backend.activities.edit') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Info</h3>
        </div>
        <div class="box-body">
            <div class="row div col-md-12">
                <table class="table table-striped table-hover">

                    <tr>
                        <th>{{ trans('labels.backend.activities.tabs.content.name') }}</th>
                        <td>{{ $activity->name }}</td>
                    </tr>
                    <tr>
                        <th>{{ trans('labels.backend.activities.tabs.content.type') }}</th>
                        <td>{{ $activity->type }}</td>
                    </tr>
                    <tr>
                        <th>{{ trans('labels.backend.activities.tabs.content.is_planning') }}</th>
                        <td>{!! $activity->plannable !!}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    {{ Form::model($activity, ['route' => ['admin.activities.update', $activity], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="box box-success">
            <div class="box-header with-border">

                <h3 class="box-title">{{ trans('labels.backend.activities.edit') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.activities.includes.partials.header-buttons')
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">

                    {{ Form::label('color', trans('validation.attributes.backend.activities.color'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <div id="color-picker" class="input-group colorpicker-component">
                            {{ Form::text('color', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.activities.color')]) }}
                            <span class="input-group-addon"><i></i></span>
                        </div>

                    </div><!--col-lg-10-->
                </div><!--form control-->

            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.activities.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@stop

@section('after-scripts')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/css/bootstrap-colorpicker.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/js/bootstrap-colorpicker.min.js"></script>
    <script>
        $('#color-picker').colorpicker({
            format: 'hex'
        });
    </script>
@stop

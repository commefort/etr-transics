@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management'))

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@stop

@section('page-header')
    <h1>
        {{ trans('labels.backend.activities.title') }}
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.activities.header') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.activities.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.access.users.table.id') }}</th>
                            <th>{{ trans('labels.backend.activities.table.color') }}</th>
                            <th>{{ trans('labels.backend.activities.table.is_planning') }}</th>
                            <th>{{ trans('labels.backend.activities.table.name') }}</th>
                            <th>{{ trans('labels.backend.activities.table.type') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.created') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.last_updated') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->renderType('Activity') !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@stop

@section('after-scripts')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}

    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.activities.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: 'transics_activities.id'},
                    {data: 'color', name: 'transics_activities.color'},
                    {data: 'is_planning', name: 'transics_activities.is_planning'},
                    {data: 'name', name: 'transics_activities.name', render: $.fn.dataTable.render.text()},
                    {data: 'type', name: 'transics_activities.type', render: $.fn.dataTable.render.text()},
                    {data: 'created_at', name: 'transics_activities.created_at'},
                    {data: 'updated_at', name: 'transics_activities.updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[2, "desc"],[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@stop

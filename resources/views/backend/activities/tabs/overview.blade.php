<table class="table table-striped table-hover">

    <tr>
        <th>{{ trans('labels.backend.activities.tabs.content.name') }}</th>
        <td>{{ $activity->name }}</td>
    </tr>
    <tr>
        <th>{{ trans('labels.backend.activities.tabs.content.type') }}</th>
        <td>{{ $activity->type }}</td>
    </tr>
    <tr>
        <th>{{ trans('labels.backend.activities.tabs.content.is_planning') }}</th>
        <td>{!! $activity->plannable !!}</td>
    </tr>
    <tr>
        <th>{{ trans('labels.backend.activities.tabs.content.color') }}</th>
        <td>{!!  $activity->color_icon !!}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.activities.tabs.content.created_at') }}</th>
        <td>{{ $activity->created_at }} ({{ $activity->created_at->diffForHumans() }})</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.activities.tabs.content.updated_at') }}</th>
        <td>{{ $activity->updated_at }} ({{ $activity->updated_at->diffForHumans() }})</td>
    </tr>
</table>
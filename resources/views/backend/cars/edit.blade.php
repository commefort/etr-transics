@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | Voertuig aanpassen')

@section('page-header')
    <h1>
        {{ trans('labels.backend.drivers.header') }}
        <small>Voertuig aanpassen</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($driver, ['route' => ['admin.car.update', $driver], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="box box-success">
            <div class="box-header with-border">

                <h3 class="box-title">Voertuig aanpassen</h3>

                <div class="box-tools pull-right">
                    @include('backend.cars.includes.partials.header-buttons')
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                {!! Form::hidden('type') !!}
                @if($driver->type == 'internal')
                    <div class="form-group">
                        {{ Form::label('hide', 'Verberg in planner?', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-3">
                            {{ Form::checkbox('hide') }}
                        </div><!--col-lg-3-->
                    </div><!--form control-->
                @else
                    <div class="form-group">
                        {{ Form::label('plate', trans('validation.attributes.backend.access.users.name'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::text('plate', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.name')]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->

                    <div class="form-group">
                        {{ Form::label('email', 'E-mail', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::text('email', null, ['class' => 'form-control']) }}
                        </div><!--col-lg-3-->
                    </div><!--form control-->

                    <div class="form-group">
                        {{ Form::label('status', trans('validation.attributes.backend.drivers.status'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-3">
                            {{ Form::select('status', ['operational' => 'Actief', 'maintenance' => 'In onderhoud', 'inactive' => 'Inactief'], null, ['class' => 'form-control']) }}
                        </div><!--col-lg-3-->
                    </div><!--form control-->
                @endif

            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.access.user.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@stop

@section('after-scripts')
    {{ Html::script('js/backend/access/users/script.js') }}
@stop

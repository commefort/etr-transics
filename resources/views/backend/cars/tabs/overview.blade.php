<table class="table table-striped table-hover">

    <tr>
        <th>{{ trans('labels.backend.drivers.tabs.content.plate') }}</th>
        <td>{{ $driver->plate }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.drivers.tabs.content.status') }}</th>
        <td>{{ $driver->trans_status }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.drivers.tabs.content.type') }}</th>
        <td>{!! $driver->trans_type !!}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.drivers.tabs.content.created_at') }}</th>
        <td>{{ $driver->created_at }} ({{ $driver->created_at->diffForHumans() }})</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.drivers.tabs.content.updated_at') }}</th>
        <td>{{ $driver->updated_at }} ({{ $driver->updated_at->diffForHumans() }})</td>
    </tr>
</table>
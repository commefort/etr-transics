@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <h3>Overzicht Bons</h3>
            <p>Deze bons worden morgen uitgevoerd (leveringen en ophalingen).</p>
            <p>&nbsp;</p>
        </div>
    </div>
    <div id="body" class="col-md-12">
        <table class="table table-hover" id="contracts-tomorow">
            <thead>
            <tr>
                <th>
                    id
                </th>
                <th>
                    Bon
                </th>
                <th>
                    Klant
                </th>
                <th>
                    Locatie
                </th>
                <th>
                    Straat
                </th>
                <th>
                    Gemeente
                </th>
                <th>
                    Land
                </th>
                <th>
                    Klant contact
                </th>
                <th style="width: 100px;">
                    Klant telefoon
                </th>
                <th>
                    Klant email
                </th>
                <th>
                    Locatie contact
                </th>
                <th style="width: 100px;">
                    Locatie telefoon
                </th>
                <th>
                    Levering
                </th>
                <th>
                    Ophaal
                </th>
                <th>
                    Artikellen
                </th>
            </tr>
            </thead>
        </table>
    </div>
    </div>
    <script>
        window.orders_tomorrow = {!! json_encode($tomorrow) !!};
    </script>
@endsection

@section('after-scripts')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.bootstrap.min.css">
    <script
            src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
            crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.2/js/dataTables.fixedHeader.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
    <script src="{{url('js/overview.js')}}"></script>
@endsection
@extends('frontend.layouts.app')

@section('content')
    <div class="row" id="calendar">


    </div><!-- row -->
@endsection

@section('after-scripts')
    {!! Html::script(elixir('js/calendar.js')) !!}
@endsection
@section('after-styles')
    <style>
        @media print {
            #calendar-list-header, .el-button-group, .el-input {
                display: none !important;
            }
            .el-table, .el-table__body, .el-table__header {
                max-width: 100% !important;
            }
        }
    </style>
@endsection
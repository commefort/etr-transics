@extends('frontend.layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12" id="planner">

        </div>
    </div>

    <script>
        window.gmaps_key = '{{env('GMAPS_KEY')}}';
        window.drivers = {!! $drivers->toJson() !!};
        window.activities = {!! $activities->toJson() !!};
    </script>

@endsection

@section('after-scripts')
    {!! Html::script(elixir('js/planner.js')) !!}
@endsection
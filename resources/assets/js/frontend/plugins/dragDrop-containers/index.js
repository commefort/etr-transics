let DnDC = {}

/**
 * By defaut, droppable will receive any group if it doesn't be especified
 */
DnDC.install = function (Vue) {
    Vue.directive('draggable', {
        bind: function (el, binding, vnode) {
            if (Object.keys(binding.modifiers).length === 0) {
                throw new Error('You have to specify almost one group in the modifier of draggable element')
            }
            el.dataset.draggable = Object.keys(binding.modifiers);

            let dragStart = (ev) => {
                ev.dataTransfer.effectAllowed = 'all';

                const data = {
                    data: binding.value,
                    group: Object.keys(binding.modifiers)
                };
                ev.dataTransfer.setData('text', JSON.stringify(data))
            }
            el.setAttribute('draggable', true)
            el.addEventListener('dragstart', dragStart)
        },
        update: function (el, binding, vnode) {
            el.removeEventListener('dragstart', {})
            if (Object.keys(binding.modifiers).length === 0) {
                throw new Error('You have to specify almost one group in the modifier of draggable element')
            }
            let dragStart = (ev) => {
                ev.dataTransfer.effectAllowed = 'all'
                const data = {
                    data: binding.value,
                    group: Object.keys(binding.modifiers)
                };
                ev.dataTransfer.setData('text', JSON.stringify(data))
            }
            el.setAttribute('draggable', true)
            //el.addEventListener('dragstart', dragStart)
        },
        unbind: function (el, binding, vnode) {
            el.setAttribute('draggable', false)
            el.removeEventListener('dragstart', {})
        }
    })

    Vue.directive('droppable', {
        bind: function (el, binding, vnode) {
            el.dataset.droppable = Object.keys(binding.modifiers);

            let drop = (ev) => {
                if (ev.preventDefault) ev.preventDefault()
                if (ev.stopPropagation) ev.stopPropagation();
                const data = JSON.parse(ev.dataTransfer.getData('text'));
                let vData = data.data;
                let myGroup = data.group;
                let imIn = false
                if (binding.modifiers.length === 0) {
                    imIn = true
                } else {
                    imIn = myGroup.find((element, index, array) => {
                        return binding.modifiers[element]
                    })
                }
                if (imIn) {
                    binding.value.handler.call(el, vData, binding.value.data);
                    ev.target.classList.remove('dragover')
                } else {
                    ev.target.classList.remove('dragover')
                }
            }
            let dragOver = (ev) => {
                if (ev.preventDefault) ev.preventDefault()
                ev.target.classList.add('dragover')

            }
            let dragLeave = (ev) => {
                if (ev.preventDefault) ev.preventDefault()
                ev.target.classList.remove('dragover')
            }
            el.addEventListener('drop', drop)
            el.addEventListener('dragover', dragOver)
            el.addEventListener('dragenter', dragOver)
            el.addEventListener('dragleave', dragLeave)
        },
        update: function (el, binding, vnode) {
            el.removeEventListener('drop', {});
            let drop = (ev) => {
                if (ev.preventDefault) ev.preventDefault()
                if (ev.stopPropagation) ev.stopPropagation()
                const data = JSON.parse(ev.dataTransfer.getData('text'));
                let vData = data.data;
                let myGroup = data.group;
                let imIn = false
                if (this._binding.modifiers.length === 0) {
                    imIn = true
                } else {
                    imIn = myGroup.find((element, index, array) => {
                        return binding.modifiers[element]
                    })
                }
                if (imIn) {
                    binding.value.handler.call(el, vData, binding.value.data)
                    ev.target.classList.remove('dragover')
                } else {
                    ev.target.classList.remove('dragover')
                }
            };
            //el.addEventListener('drop', drop)
        },
        unbind: function (el, binding, vnode) {
            el.setAttribute('draggable', false)
            el.removeEventListener('drop', {})
            el.removeEventListener('dragover', {})
            el.removeEventListener('dragenter', {})
            el.removeEventListener('dragleave', {})
        }
    })
}

module.exports = DnDC

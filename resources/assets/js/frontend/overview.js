
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */
require('bootstrap-sass');

$(function(){
    var today = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    var day = (today.getDate() < 10) ? '0'+today.getDate() : today.getDate();
    var month = (today.getMonth()+1 < 10) ? '0'+(today.getMonth()+1) : today.getMonth()+1;

    var date = day+'/'+month+'/'+today.getFullYear();


    $('#contracts-tomorow').DataTable({
        data: window.orders_tomorrow,
        columns: [
            { data: 'id' },
            { data: 'order_number' },
            { data: 'customer' },
            { data: 'location' },
            { data: 'street' },
            { data: 'postal' },
            { data: 'country' },
            { data: 'customer_contact' },
            { data: 'customer_contact_phone' },
            { data: 'customer_contact_email' },
            { data: 'location_contact' },
            { data: 'location_contact_phone' },
            {
                data: 'transport_in',
                render: function( data, type, row) {
                    var checked = (row.pickup == '1' && data == date) ? 'check-square-o' : false;

                    if(checked !== false)
                    {
                        return data + '<p class="text-center"><i class="text-center fa fa-'+checked+'"></i></p>';
                    }
                    return data;
                }
            },
            {
                data: 'transport_out',
                render: function( data, type, row) {
                    var checked = (row.delivery == '1' && data == date) ? 'check-square-o' : false;

                    if(checked !== false)
                    {
                        return data + '<p class="text-center"><i class="text-center fa fa-'+checked+'"></i></p>';
                    }
                    return data;
                }
            },
            { data: 'order_details' }
        ],
        stateSave: true,
        scrollY: 500,
        scrollX: true,
        scrollCollapse: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        fixedHeader: true
    });
});